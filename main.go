package main

import (
	"app/config"
	"app/db"
	"app/web"
	"flag"
	"fmt"
	"time"

	"gitlab.com/gbh007/gojlog"
)

func main() {
	// сбрасываем логи
	config.StartupShortLog.Reset()
	// загружаем аргументы запуска
	configPath := flag.String("c", "config.json", "файл конфигурации")
	staticDir := flag.String("s", "static", "папка с файлами для раздачи веб сервера")
	webPort := flag.Int("p", 80, "порт веб сервера")
	filesDir := flag.String("f", "files", "папка с файлами")
	flag.Parse()
	// добавляем путь отсечения логгера
	gojlog.AddTrimPathAuto()
	config.StartupShortLog.Warning(time.Now().Format(time.RFC1123))
	config.StartupShortLog.Warning("запуск чата")
	// загружаем конфигурацию
	if config.Load(*configPath) != nil {
		gojlog.Critical("ошибка загрузки конфигурации")
		return
	}
	config.StartupShortLog.Info("завершение загрузки конфигурации")
	cnf := config.GetConfig()
	// подключаемся к базе с данными
	if err := db.Connect(cnf.Conn); err != nil {
		gojlog.Critical("ошибка соединения с базой")
		return
	}
	config.StartupShortLog.Info("завершение подключения к базе данных")
	// запускаем веб сервер
	done := web.Run(fmt.Sprintf(":%d", *webPort), *staticDir, *filesDir)
	config.StartupShortLog.Info("завершение запуска веб сервера")
	StartRobot(*filesDir)
	config.StartupShortLog.Info("завершение запуска робота")
	// сохраняем лог загрузки
	config.SaveStartupShortLog()
	// ждем :-)
	gojlog.Warning("чат запущен")
	<-done
	gojlog.Warning("чат остановлен")
}
