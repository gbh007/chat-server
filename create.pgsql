-- удаление схемы СО ВСЕМИ ДАННЫМИ
DROP SCHEMA IF EXISTS main CASCADE;

-- создание схемы для данных
CREATE SCHEMA IF NOT EXISTS main;

-- создание пользователей
CREATE TABLE main.users(
    id SERIAL PRIMARY KEY,
    firstname TEXT NOT NULL,
    lastname TEXT NOT NULL,
    patronimyc TEXT NOT NULL,
    last_action TIMESTAMPTZ NOT NULL,
    token TEXT UNIQUE,
    aes TEXT UNIQUE,
    admin BOOL DEFAULT FALSE,
    -- avatar TEXT REFERENCES main.files(file) ON UPDATE CASCADE DEFAULT NULL,
    color TEXT NOT NULL DEFAULT '#000000',
    UNIQUE(firstname, lastname, patronimyc)
);

-- создание груп
CREATE TABLE main.groups(
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    owner_id INT NOT NULL REFERENCES main.users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    public BOOL NOT NULL -- общедоступная группа
);

-- создание связи груп и пользователей
CREATE TABLE main.link_user_group(
    user_id INT NOT NULL REFERENCES main.users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    group_id INT NOT NULL REFERENCES main.groups(id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- создание сообщений
CREATE TABLE main.messages(
    id BIGSERIAL PRIMARY KEY,
    date TIMESTAMPTZ NOT NULL,
    text TEXT NOT NULL,
    sender_id INT NOT NULL REFERENCES main.users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    user_id INT REFERENCES main.users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    group_id INT REFERENCES main.groups(id) ON UPDATE CASCADE ON DELETE CASCADE,
    private_id INT REFERENCES main.users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    readed BOOL NOT NULL -- сообщение прочитано
);

-- создание файлов
CREATE TABLE main.files(
    file TEXT PRIMARY KEY,
    name TEXT,
    sticker_name TEXT UNIQUE,
    mime TEXT,
    date TIMESTAMPTZ NOT NULL,
    uploader_id INT NOT NULL REFERENCES main.users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    private BOOL DEFAULT FALSE,
    size INT,
    is_avatar BOOL DEFAULT FALSE
);

-- добавление поля аватара у пользователя
-- идет после всех объявлений из-за пересечения линков таблиц
ALTER TABLE main.users ADD COLUMN avatar TEXT REFERENCES main.files(file) ON UPDATE CASCADE DEFAULT NULL;