module app

go 1.15

require (
	github.com/lib/pq v1.10.2
	gitlab.com/gbh007/gojlog v1.3.1
)
