all: go-build react-install react-build

build: go-build react-build

go-build:
	go build -o main

react-install:
	cd frontend && npm install

react-build:
	cd frontend && npm run build

run: go-build react-build
	./main -p 8080 -c .hidden/config.json -s frontend/build -f .hidden/files

start:
	cd frontend && npm start

build-alpine:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o main

docker: build-alpine react-build
	docker tag chat-server:latest chat-server:old || true
	docker build -t chat-server:latest .
	docker stop chat-server || true && docker rm chat-server || true
	docker run -p 20000:80 -d --name chat-server --restart always -v /etc/chat-server:/app/cnf -v /var/log/chat-server:/app/log -v /var/lib/chat-server:/app/files chat-server:latest
	docker rmi chat-server:old || true