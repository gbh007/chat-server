package main

import (
	"app/db"
	"os"
	"path"
	"regexp"
	"time"

	"gitlab.com/gbh007/gojlog"
)

// clearInterval интервал отчситки файлов на сервере
const clearInterval = time.Hour

// oldFileInterval время хранения файлов (90 дней)
const oldFileInterval = time.Hour * 24 * 30 * 3

// StartRobot запускает автоотчистку в фоновом процессе
func StartRobot(filesDir string) {
	go runRobot(filesDir)
}

// runRobot запускает автоотчистку
func runRobot(filesDir string) {
	clear := func() {
		dir, err := os.Open(filesDir)
		if err != nil {
			gojlog.Error(err)
			return
		}
		files, err := dir.Readdirnames(0)
		if err != nil {
			gojlog.Error(err)
			return
		}
		var notInBaseCount, oldFileCount, oldAvatarCount int
		// удаление файлов что отсутствуют в базе (только по совпадению формата файла - ключа)
		for _, file := range files {
			if ok, err := regexp.MatchString(`^[\w\d]{32}$`, file); !ok || err != nil {
				continue
			}
			if ok, err := db.FileExists(file); !ok && err == nil {
				err := os.Remove(path.Join(filesDir, file))
				if err != nil {
					gojlog.Error(err)
					continue
				} else {
					notInBaseCount++
				}
			}
		}
		// удаление старых файлов
		for _, file := range db.SelectOldFiles(time.Now().Add(-oldFileInterval)) {
			err := db.DeleteFileByFilename(file.Filename)
			if err != nil {
				gojlog.Error(err)
				continue
			}
			err = os.Remove(path.Join(filesDir, file.Filename))
			if err != nil {
				gojlog.Error(err)
				continue
			} else {
				oldFileCount++
			}
		}
		// удаление старых аватаров
		for _, file := range db.SelectInvalidAvatars() {
			err := db.DeleteFileByFilename(file.Filename)
			if err != nil {
				gojlog.Error(err)
				continue
			}
			err = os.Remove(path.Join(filesDir, file.Filename))
			if err != nil {
				gojlog.Error(err)
				continue
			} else {
				oldAvatarCount++
			}
		}
		// вывод информации в лог
		if notInBaseCount+oldFileCount+oldAvatarCount > 0 {
			gojlog.Infof(
				"удалено: %d файлов вне базы, %d старых файлов, %d старых аватаров",
				notInBaseCount,
				oldFileCount,
				oldAvatarCount,
			)
		}
	}
	clear()
	for range time.NewTicker(clearInterval).C {
		clear()
	}
}
