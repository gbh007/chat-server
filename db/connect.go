package db

import (
	"database/sql"

	_ "github.com/lib/pq"
)

var _db *sql.DB

func GetDatabase() *sql.DB {
	return _db
}

// Connect подключение к базе
func Connect(conn string) (err error) {
	_db, err = sql.Open("postgres", conn)
	if err != nil {
		return err
	}
	return _db.Ping()
}
