package db

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"io"

	"gitlab.com/gbh007/gojlog"
)

// encode шифрует сообщение
func encode(rkey, rtext string) (string, error) {
	key := []byte(rkey)
	text := []byte(rtext)
	cphr, err := aes.NewCipher(key)
	if err != nil {
		gojlog.Error(err)
		return "", err
	}
	gcm, err := cipher.NewGCM(cphr)
	if err != nil {
		gojlog.Error(err)
		return "", err
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		gojlog.Error(err)
		return "", err
	}
	return base64.StdEncoding.EncodeToString(gcm.Seal(nonce, nonce, text, nil)), nil
}

// decode дешифрует сообщение
func decode(rkey, rtext string) (string, error) {
	key := []byte(rkey)
	ciphertext, err := base64.StdEncoding.DecodeString(rtext)
	if err != nil {
		gojlog.Error(err)
		return "", err
	}
	c, err := aes.NewCipher(key)
	if err != nil {
		gojlog.Error(err)
		return "", err
	}
	gcmDecrypt, err := cipher.NewGCM(c)
	if err != nil {
		gojlog.Error(err)
		return "", err
	}
	nonceSize := gcmDecrypt.NonceSize()
	if len(ciphertext) < nonceSize {
		gojlog.Error(err)
		return "", err
	}
	nonce, encryptedMessage := ciphertext[:nonceSize], ciphertext[nonceSize:]
	plaintext, err := gcmDecrypt.Open(nil, nonce, encryptedMessage, nil)
	if err != nil {
		gojlog.Error(err)
		return "", err
	}
	return string(plaintext), nil
}
