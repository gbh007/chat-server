package db

import (
	"database/sql"
	"time"

	"gitlab.com/gbh007/gojlog"
)

type User struct {
	ID         int       `json:"id"`
	Lastname   string    `json:"lastname"`
	Firstname  string    `json:"firstname"`
	Patronimyc string    `json:"patronimyc"`
	Token      string    `json:"token"`
	Admin      bool      `json:"admin"`
	LastAction time.Time `json:"last_action"`
	AES        string    `json:"aes"`
	Avatar     string    `json:"avatar"`
	Color      string    `json:"color"`
}

// InsertUser добавляет нового пользователя в базу
func InsertUser(u User) (int, error) {
	avatar := sql.NullString{String: u.Avatar, Valid: u.Avatar != ""}
	// внесение данных
	row := _db.QueryRow(`INSERT INTO main.users 
	(lastname, firstname, patronimyc, token, last_action, admin, aes, avatar, color) 
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING id`,
		u.Lastname,
		u.Firstname,
		u.Patronimyc,
		u.Token,
		u.LastAction,
		u.Admin,
		u.AES,
		avatar,
		u.Color,
	)
	var id int
	err := row.Scan(&id)
	gojlog.IfErr(err)
	return id, err
}

// SelectUsers возвращает всех пользователей
func SelectUsers() []User {
	result := make([]User, 0)
	rows, err := _db.Query(`SELECT id, lastname, firstname, patronimyc, token, last_action, admin, aes, avatar, color FROM main.users ORDER BY id`)
	if err != nil {
		gojlog.Error(err)
		return result
	}
	for rows.Next() {
		u := User{}
		avatar := sql.NullString{}
		err := rows.Scan(
			&u.ID,
			&u.Lastname,
			&u.Firstname,
			&u.Patronimyc,
			&u.Token,
			&u.LastAction,
			&u.Admin,
			&u.AES,
			&avatar,
			&u.Color,
		)
		if err != nil {
			gojlog.Error(err)
		} else {
			u.Avatar = avatar.String
			result = append(result, u)
		}
	}
	return result
}

// SelectUserByToken возвращает пользователя по токену
func SelectUserByToken(token string) (User, error) {
	row := _db.QueryRow(`SELECT id, lastname, firstname, patronimyc, token, last_action, admin, aes, avatar, color FROM main.users WHERE token = $1`, token)
	u := User{}
	avatar := sql.NullString{}
	err := row.Scan(
		&u.ID,
		&u.Lastname,
		&u.Firstname,
		&u.Patronimyc,
		&u.Token,
		&u.LastAction,
		&u.Admin,
		&u.AES,
		&avatar,
		&u.Color,
	)
	u.Avatar = avatar.String
	if err != sql.ErrNoRows {
		gojlog.IfErr(err)
	}
	return u, err
}

// SelectUserByID возвращает пользователя по ИД
func SelectUserByID(id int) (User, error) {
	row := _db.QueryRow(`SELECT id, lastname, firstname, patronimyc, token, last_action, admin, aes, avatar, color FROM main.users WHERE id = $1`, id)
	u := User{}
	avatar := sql.NullString{}
	err := row.Scan(
		&u.ID,
		&u.Lastname,
		&u.Firstname,
		&u.Patronimyc,
		&u.Token,
		&u.LastAction,
		&u.Admin,
		&u.AES,
		&avatar,
		&u.Color,
	)
	u.Avatar = avatar.String
	if err != sql.ErrNoRows {
		gojlog.IfErr(err)
	}
	return u, err
}

// SelectUsersInGroup возвращает всех пользователей в группе
func SelectUsersInGroup(groupID int) []User {
	result := make([]User, 0)
	rows, err := _db.Query(`
	SELECT u.id, u.lastname, u.firstname, u.patronimyc, u.token, u.last_action, u.admin, u.aes, u.avatar, u.color
	FROM main.users AS u INNER JOIN main.link_user_group AS l ON l.user_id = u.id
	WHERE l.group_id = $1
	ORDER BY id`, groupID)
	if err != nil {
		gojlog.Error(err)
		return result
	}
	for rows.Next() {
		u := User{}
		avatar := sql.NullString{}
		err := rows.Scan(
			&u.ID,
			&u.Lastname,
			&u.Firstname,
			&u.Patronimyc,
			&u.Token,
			&u.LastAction,
			&u.Admin,
			&u.AES,
			&avatar,
			&u.Color,
		)
		if err != nil {
			gojlog.Error(err)
		} else {
			u.Avatar = avatar.String
			result = append(result, u)
		}
	}
	return result
}

// DeleteUserFromGroup удаляет пользователя из группы
func DeleteUserFromGroup(userID, groupID int) error {
	// внесение данных
	_, err := _db.Exec(`DELETE FROM main.link_user_group WHERE user_id = $1 AND group_id = $2`,
		userID, groupID,
	)
	gojlog.IfErr(err)
	return err
}

// InsertUserInGroup добавляет нового пользователя в группу
func InsertUserInGroup(userID, groupID int) error {
	// внесение данных
	_, err := _db.Exec(`INSERT INTO main.link_user_group (user_id, group_id) VALUES ($1, $2)`,
		userID, groupID,
	)
	gojlog.IfErr(err)
	return err
}

// UpdateUserName обновляет ФИО пользователя
func UpdateUserName(userID int, lastname, firstname, patronimyc string) error {
	_, err := _db.Exec(`
	UPDATE main.users 
	SET lastname = $2, firstname = $3, patronimyc = $4
	WHERE
		id = $1`, userID, lastname, firstname, patronimyc)
	gojlog.IfErr(err)
	return err
}

// UpdateColor обновляет цвет пользователя
func UpdateColor(userID int, color string) error {
	_, err := _db.Exec(`
	UPDATE main.users 
	SET color = $2
	WHERE
		id = $1`, userID, color)
	gojlog.IfErr(err)
	return err
}

// UpdateAvatar обновляет аватар пользователя
func UpdateAvatar(userID int, avatar string) error {
	_, err := _db.Exec(`
	UPDATE main.users 
	SET avatar = $2
	WHERE
		id = $1`, userID, avatar)
	gojlog.IfErr(err)
	return err
}

// UpdateUserLA обновляет время последнего действия пользователя
func UpdateUserLA(userID int) error {
	_, err := _db.Exec(`
	UPDATE main.users 
	SET last_action = $2
	WHERE
		id = $1`, userID, time.Now())
	gojlog.IfErr(err)
	return err
}
