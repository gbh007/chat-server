package db

import (
	"database/sql"
	"time"

	"gitlab.com/gbh007/gojlog"
)

// Message сообщение в чате
type Message struct {
	ID        int64     `json:"id"`
	Date      time.Time `json:"date"`
	Text      string    `json:"text"`
	SenderID  int       `json:"sender_id"`
	UserID    int       `json:"user_id"`
	GroupID   int       `json:"group_id"`
	PrivateID int       `json:"private_id"`
	Readed    bool      `json:"readed"`
}

// MessageWithSender сообщение с данными отправителя в чате
type MessageWithSender struct {
	Message
	Sender User `json:"sender"`
}

// InsertMessage добавляет новое сообщение в базу
func InsertMessage(m Message) error {
	// шифрование данных
	u, err := SelectUserByID(m.UserID)
	if err != nil {
		return err
	}
	m.Text, err = encode(u.AES, m.Text)
	if err != nil {
		return err
	}
	// расчет сообщения
	groupID := sql.NullInt32{Int32: int32(m.GroupID), Valid: m.GroupID > 0}
	privateID := sql.NullInt32{Int32: int32(m.PrivateID), Valid: m.PrivateID > 0}
	// внесение данных
	_, err = _db.Exec(`INSERT INTO main.messages (date, text, sender_id, user_id, group_id, private_id, readed) VALUES ($1, $2, $3, $4, $5, $6, $7)`,
		m.Date,
		m.Text,
		m.SenderID,
		m.UserID,
		groupID,
		privateID,
		m.Readed,
	)
	gojlog.IfErr(err)
	return err
}

// SelectPrivateMessages возвращает личные сообщения по получателю и отправителю
func SelectPrivateMessages(userID, senderID int) []MessageWithSender {
	result := make([]MessageWithSender, 0)
	rows, err := _db.Query(`
	SELECT 
		m.id, m.date, m.text, m.sender_id, m.user_id, m.readed, m.private_id,
		u.id, u.lastname, u.firstname, u.patronimyc, u.avatar, u.color
	FROM main.messages AS m INNER JOIN main.users AS u ON m.sender_id = u.id
	WHERE
		m.user_id = $1 AND m.private_id = $2 AND m.group_id IS NULL
	ORDER BY date`, userID, senderID)
	if err != nil {
		gojlog.Error(err)
		return result
	}
	for rows.Next() {
		m := MessageWithSender{}
		avatar := sql.NullString{}
		err := rows.Scan(
			&m.ID,
			&m.Date,
			&m.Text,
			&m.SenderID,
			&m.UserID,
			&m.Readed,
			&m.PrivateID,
			&m.Sender.ID,
			&m.Sender.Lastname,
			&m.Sender.Firstname,
			&m.Sender.Patronimyc,
			&avatar,
			&m.Sender.Color,
		)
		if err != nil {
			gojlog.Error(err)
		} else {
			u, err := SelectUserByID(m.UserID)
			if err != nil {
				continue
			}
			m.Text, err = decode(u.AES, m.Text)
			if err != nil {
				continue
			}
			m.Sender.Avatar = avatar.String
			result = append(result, m)
		}
	}
	return result
}

// SelectCountPrivateMessages возвращает количество личных сообщения по получателю
func SelectCountPrivateMessages(userID int) map[int]int {
	result := make(map[int]int)
	rows, err := _db.Query(`
	SELECT 
		m.private_id,
		COUNT(m.private_id)
	FROM main.messages AS m
	WHERE
		m.user_id = $1 AND m.readed = false AND m.group_id IS NULL
	GROUP BY m.private_id
	`, userID)
	if err != nil {
		gojlog.Error(err)
		return result
	}
	for rows.Next() {
		var id, count int
		err := rows.Scan(&id, &count)
		if err != nil {
			gojlog.Error(err)
		} else {
			result[id] = count
		}
	}
	return result
}

// UpdateReadPrivateMessages обновляет статус прочитаных сообщений
func UpdateReadPrivateMessages(userID, senderID int) error {
	_, err := _db.Exec(`
	UPDATE main.messages 
	SET readed = TRUE
	WHERE
		user_id = $1 AND private_id = $2 
		AND group_id IS NULL`, userID, senderID)
	gojlog.IfErr(err)
	return err
}

// SelectGroupMessages возвращает груповые сообщения по получателю и группы
func SelectGroupMessages(userID, groupID int) []MessageWithSender {
	result := make([]MessageWithSender, 0)
	rows, err := _db.Query(`
	SELECT 
		m.id, m.date, m.text, m.sender_id, m.user_id, m.group_id, m.readed,
		u.id, u.lastname, u.firstname, u.patronimyc, u.avatar, u.color
	FROM main.messages AS m INNER JOIN main.users AS u ON m.sender_id = u.id
	WHERE
		m.user_id = $1 AND m.group_id = $2 AND m.private_id IS NULL
	ORDER BY date`, userID, groupID)
	if err != nil {
		gojlog.Error(err)
		return result
	}
	for rows.Next() {
		m := MessageWithSender{}
		avatar := sql.NullString{}
		err := rows.Scan(
			&m.ID,
			&m.Date,
			&m.Text,
			&m.SenderID,
			&m.UserID,
			&m.GroupID,
			&m.Readed,
			&m.Sender.ID,
			&m.Sender.Lastname,
			&m.Sender.Firstname,
			&m.Sender.Patronimyc,
			&avatar,
			&m.Sender.Color,
		)
		if err != nil {
			gojlog.Error(err)
		} else {
			u, err := SelectUserByID(m.UserID)
			if err != nil {
				continue
			}
			m.Text, err = decode(u.AES, m.Text)
			if err != nil {
				continue
			}
			m.Sender.Avatar = avatar.String
			result = append(result, m)
		}
	}
	return result
}

// SelectCountGroupMessages возвращает количество сообщений в группе
func SelectCountGroupMessages(userID int) map[int]int {
	result := make(map[int]int)
	rows, err := _db.Query(`
	SELECT 
		m.group_id,
		COUNT(m.group_id)
	FROM main.messages AS m
	WHERE
		m.user_id = $1 AND m.readed = false AND m.private_id IS NULL
	GROUP BY m.group_id
	`, userID)
	if err != nil {
		gojlog.Error(err)
		return result
	}
	for rows.Next() {
		var id, count int
		err := rows.Scan(&id, &count)
		if err != nil {
			gojlog.Error(err)
		} else {
			result[id] = count
		}
	}
	return result
}

// UpdateReadGroupMessages обновляет статус прочитаных сообщений в группе
func UpdateReadGroupMessages(userID, groupID int) error {
	_, err := _db.Exec(`
	UPDATE main.messages 
	SET readed = TRUE
	WHERE
		user_id = $1 AND group_id = $2`, userID, groupID)
	gojlog.IfErr(err)
	return err
}
