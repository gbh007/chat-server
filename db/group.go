package db

import (
	"database/sql"

	"gitlab.com/gbh007/gojlog"
)

type Group struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	OwnerID int    `json:"owner_id"`
	Public  bool   `json:"public"`
}

// InsertGroup добавляет новую группу в базу
func InsertGroup(g Group) (int, error) {
	// внесение данных
	row := _db.QueryRow(`INSERT INTO main.groups (name, owner_id, public) VALUES ($1, $2, $3) RETURNING id`,
		g.Name,
		g.OwnerID,
		g.Public,
	)
	var id int
	err := row.Scan(&id)
	gojlog.IfErr(err)
	return id, err
}

// SelectGroupByID возвращает группу по ИД
func SelectGroupByID(id int) (Group, error) {
	row := _db.QueryRow(`SELECT id, name, owner_id, public FROM main.groups WHERE id = $1`, id)
	g := Group{}
	err := row.Scan(
		&g.ID,
		&g.Name,
		&g.OwnerID,
		&g.Public,
	)
	if err != sql.ErrNoRows {
		gojlog.IfErr(err)
	}
	return g, err
}

// SelectGroups возвращает все группы
func SelectGroups(userID int) []Group {
	result := make([]Group, 0)
	rows, err := _db.Query(`
	SELECT g.id, g.name, g.owner_id, g.public
	FROM main.groups AS g INNER JOIN main.link_user_group AS l ON l.group_id = g.id
	WHERE l.user_id = $1
	GROUP BY g.id, g.name, g.owner_id, g.public
	ORDER BY g.id`, userID)
	if err != nil {
		gojlog.Error(err)
		return result
	}
	for rows.Next() {
		g := Group{}
		err := rows.Scan(
			&g.ID,
			&g.Name,
			&g.OwnerID,
			&g.Public,
		)
		if err != nil {
			gojlog.Error(err)
		} else {
			result = append(result, g)
		}
	}
	return result
}

// SelectPublicGroups возвращает все публичные группы
func SelectPublicGroups() []Group {
	result := make([]Group, 0)
	rows, err := _db.Query(`
	SELECT g.id, g.name, g.owner_id, g.public
	FROM main.groups AS g 
	WHERE g.public = true
	ORDER BY g.id`)
	if err != nil {
		gojlog.Error(err)
		return result
	}
	for rows.Next() {
		g := Group{}
		err := rows.Scan(
			&g.ID,
			&g.Name,
			&g.OwnerID,
			&g.Public,
		)
		if err != nil {
			gojlog.Error(err)
		} else {
			result = append(result, g)
		}
	}
	return result
}

// UpdateGroupName обновляет название группы
func UpdateGroupName(groupID int, name string) error {
	_, err := _db.Exec(`
	UPDATE main.groups 
	SET name = $2
	WHERE
		id = $1`, groupID, name)
	gojlog.IfErr(err)
	return err
}

// DeleteGroup удаляет группу
func DeleteGroup(groupID int) error {
	_, err := _db.Exec(`DELETE FROM main.groups WHERE id = $1`, groupID)
	gojlog.IfErr(err)
	return err
}
