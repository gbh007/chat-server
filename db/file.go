package db

import (
	"database/sql"
	"time"

	"gitlab.com/gbh007/gojlog"
)

type File struct {
	Filename    string    `json:"file"`
	Name        string    `json:"name"`
	StickerName string    `json:"sticker_name"`
	Mime        string    `json:"mime"` // MIME type (тип файла)
	Date        time.Time `json:"date"`
	UploaderID  int       `json:"uploader_id"`
	Size        int64     `json:"size"`
	Private     bool      `json:"private"`
	IsAvatar    bool      `json:"is_avatar"`
}

// InsertFile добавляет новый файл в базу
func InsertFile(f File) error {
	sname := sql.NullString{String: f.StickerName, Valid: f.StickerName != ""}
	// внесение данных
	_, err := _db.Exec(`INSERT INTO main.files (file, name, sticker_name, mime, date, uploader_id, size, private, is_avatar) 
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`,
		f.Filename,
		f.Name,
		sname,
		f.Mime,
		f.Date,
		f.UploaderID,
		f.Size,
		f.Private,
		f.IsAvatar,
	)
	gojlog.IfErr(err)
	return err
}

// SelectFileByFilename возвращает файл по названию файла на сервере
func SelectFileByFilename(filename string) (File, error) {
	sname := sql.NullString{}
	row := _db.QueryRow(`SELECT file, name, sticker_name, mime, date, uploader_id, size, private, is_avatar FROM main.files WHERE file = $1`, filename)
	f := File{}
	err := row.Scan(
		&f.Filename,
		&f.Name,
		&sname,
		&f.Mime,
		&f.Date,
		&f.UploaderID,
		&f.Size,
		&f.Private,
		&f.IsAvatar,
	)
	f.StickerName = sname.String
	if err != sql.ErrNoRows {
		gojlog.IfErr(err)
	}
	return f, err
}

// SelectFileByStickerName возвращает файл по названию стикера на сервере
func SelectFileByStickerName(stickerName string) (File, error) {
	row := _db.QueryRow(`SELECT file, name, sticker_name, mime, date, uploader_id, size, private, is_avatar FROM main.files WHERE sticker_name = $1`, stickerName)
	f := File{}
	err := row.Scan(
		&f.Filename,
		&f.Name,
		&f.StickerName,
		&f.Mime,
		&f.Date,
		&f.UploaderID,
		&f.Size,
		&f.Private,
		&f.IsAvatar,
	)
	if err != sql.ErrNoRows {
		gojlog.IfErr(err)
	}
	return f, err
}

// SelectStickers возвращает все стикеры из базы
func SelectStickers() []File {
	result := make([]File, 0)
	rows, err := _db.Query(`SELECT file, name, sticker_name, mime, date, uploader_id, size, private, is_avatar
	FROM main.files 
	WHERE sticker_name IS NOT NULL`)
	if err != nil {
		gojlog.Error(err)
		return result
	}
	for rows.Next() {
		f := File{}
		err := rows.Scan(
			&f.Filename,
			&f.Name,
			&f.StickerName,
			&f.Mime,
			&f.Date,
			&f.UploaderID,
			&f.Size,
			&f.Private,
			&f.IsAvatar,
		)
		if err != nil {
			gojlog.Error(err)
		} else {
			result = append(result, f)
		}
	}
	return result
}

// SelectInvalidAvatars возвращает все некоректные аватары
func SelectInvalidAvatars() []File {
	result := make([]File, 0)
	rows, err := _db.Query(`SELECT f.file, f.name, f.mime, f.date, f.uploader_id, f.size, f.private, f.is_avatar
	FROM main.files AS f LEFT JOIN main.users AS u ON f.file  = u.avatar 
WHERE f.is_avatar = TRUE AND u.lastname IS NULL`)
	if err != nil {
		gojlog.Error(err)
		return result
	}
	for rows.Next() {
		f := File{}
		err := rows.Scan(
			&f.Filename,
			&f.Name,
			&f.Mime,
			&f.Date,
			&f.UploaderID,
			&f.Size,
			&f.Private,
			&f.IsAvatar,
		)
		if err != nil {
			gojlog.Error(err)
		} else {
			result = append(result, f)
		}
	}
	return result
}

// SelectOldFiles возвращает все файлы старше даты
func SelectOldFiles(date time.Time) []File {
	result := make([]File, 0)
	rows, err := _db.Query(`SELECT f.file, f.name, f.mime, f.date, f.uploader_id, f.size, f.private, f.is_avatar
	FROM main.files AS f
WHERE f.is_avatar = FALSE AND f.date < $1 AND f.sticker_name IS NULL`, date)
	if err != nil {
		gojlog.Error(err)
		return result
	}
	for rows.Next() {
		f := File{}
		err := rows.Scan(
			&f.Filename,
			&f.Name,
			&f.Mime,
			&f.Date,
			&f.UploaderID,
			&f.Size,
			&f.Private,
			&f.IsAvatar,
		)
		if err != nil {
			gojlog.Error(err)
		} else {
			result = append(result, f)
		}
	}
	return result
}

// FileExists проверяет файл на существования в базе
func FileExists(name string) (bool, error) {
	count := 0
	row := _db.QueryRow(`SELECT COUNT(file) FROM main.files WHERE file = $1`, name)
	err := row.Scan(&count)
	if err != nil {
		gojlog.Error(err)
		return false, err
	}
	return count > 0, nil
}

// DeleteFileByFilename удаляет файл по названию (main.files.file)
func DeleteFileByFilename(name string) error {
	_, err := _db.Exec(`DELETE FROM main.files WHERE file = $1`, name)
	if err != nil {
		gojlog.Error(err)
		return err
	}
	return nil
}
