import React from "react";

// контекст для аккаунта пользователя
export const AccountContext = React.createContext({
  login: () => {}, // вход
  logout: () => {}, // выход
  update: () => {}, // обновление данных пользователя
  accountInfo: {}, // информация о пользователе
  logged: false, // авторизирован
});

export const IsMobile =
  /Mobile|webOS|BlackBerry|IEMobile|MeeGo|mini|Fennec|Windows Phone|Android|iP(ad|od|hone)/i.test(
    navigator.userAgent
  );
