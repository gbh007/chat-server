import { Redirect, Route, Switch } from "react-router-dom";
import { LoginPage, RegistrationPage } from "./account";
import { AccountContext } from "./context";
import "./style.css";
import { useContext } from "react";
import { ChatPage } from "./chat/chat";

export function Pages() {
  const account = useContext(AccountContext);
  return (
    <Switch>
      <Route path="/login">
        {account.logged ? <Redirect to="/" /> : <LoginPage />}
      </Route>
      <Route path="/registration">
        {account.logged ? <Redirect to="/" /> : <RegistrationPage />}
      </Route>
      {/* главная страница, да она должна идти после 404 для правильной маршрутизации */}
      <Route path="/">
        {account.logged ? (
          <div id="father">
            <ChatPage />
          </div>
        ) : (
          <LoginPage />
        )}
      </Route>
    </Switch>
  );
}
