import React from "react";
import { HashRouter } from "react-router-dom";
import { AccountContext } from "./context";
import axios from "axios";
import { Pages } from "./pages";

const updatePeriod = 10 * 60 * 1000; // время автообновления данных

export class App extends React.Component {
  constructor(props) {
    super(props);
    // привязка ключевых функций
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.update = this.update.bind(this);
    // установка глобального состояния
    this.state = {
      account: this.getAccount(),
    };
  }
  componentDidMount() {
    this.update();
    this.updaterID = setInterval(this.update, updatePeriod);
  }
  componentWillUnmount() {
    clearInterval(this.updaterID);
  }
  getAccount(info = null) {
    let acc = {
      accountInfo: {},
      logged: false,
      login: this.login,
      logout: this.logout,
      update: this.update,
    };
    if (info != null) {
      acc.logged = true;
      acc.accountInfo = info;
    }
    return acc;
  }
  login(token) {
    return new Promise((resolve) => {
      axios
        .post("/account/login", JSON.stringify(token))
        .then(() => {
          this.update();
          resolve();
        })
        .catch((err) =>
          alert(err.response.statusText + ":\n" + err.response.data)
        );
    });
  }
  logout() {
    axios
      .post("/account/logout")
      .then(() => {
        this.update();
      })
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  }
  update() {
    axios
      .post("/account/info")
      .then((data) => {
        this.setState({ account: this.getAccount(data.data) });
      })
      .catch(() => {
        this.setState({ account: this.getAccount(null) });
      });
  }
  render() {
    return (
      <HashRouter>
        <AccountContext.Provider value={this.state.account}>
          <Pages />
        </AccountContext.Provider>
      </HashRouter>
    );
  }
}
