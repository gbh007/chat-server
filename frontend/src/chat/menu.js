import { useContext, useState } from "react";
import { AccountContext } from "../context";
import { RenameAccount } from "./rename-account";
import { SetAvatar } from "./set-avatar";
import { CreateSticker } from "./sticker-create";

export function Menu(props) {
  const account = useContext(AccountContext);
  const [showRename, setShowRename] = useState(false);
  const [showSticker, setShowSticker] = useState(false);
  const [showAvatar, setShowAvatar] = useState(false);
  if (!account.logged) {
    return null;
  }
  const user = account.accountInfo;
  return (
    <>
      <span id="menu">
        <span onClick={() => setShowRename(true)}>Изменить ФИО</span>
        {showRename ? (
          <RenameAccount close={() => setShowRename(false)} />
        ) : null}
        <span onClick={() => setShowAvatar(true)}>Изменить Аватар</span>
        {showAvatar ? <SetAvatar close={() => setShowAvatar(false)} /> : null}
        <span>
          {props.audio ? (
            <img
              onClick={() => props.switchAudio(!props.audio)}
              style={{ verticalAlign: "middle" }}
              src="audio_on.png"
              alt="on"
              title="Отключить звук"
            />
          ) : (
            <img
              onClick={() => props.switchAudio(!props.audio)}
              style={{ verticalAlign: "middle" }}
              src="audio_off.png"
              alt="off"
              title="Включить звук"
            />
          )}
          <input
            type="range"
            style={{ verticalAlign: "middle" }}
            min={0}
            max={1}
            step={0.01}
            value={props.volume}
            onChange={props.setVolume}
          />
        </span>
        <span onClick={() => setShowSticker(true)}>Добавить стикер</span>
        {showSticker ? (
          <CreateSticker close={() => setShowSticker(false)} />
        ) : null}
      </span>
      <span id="account">
        <b>{user.lastname + " " + user.firstname + " " + user.patronimyc}</b>
        <button onClick={account.logout} style={{ marginLeft: "10px" }}>
          Выйти
        </button>
      </span>
    </>
  );
}
