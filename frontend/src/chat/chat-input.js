import axios from "axios";
import { useContext, useState } from "react";
import { SelectSticker } from "./sticker-select";
import { AccountContext } from "../context";
import { Help } from "./help";
import { FileUpload } from "./file-upload";

export function ChatInput(props) {
  const [text, setText] = useState("");
  const [showSticker, setShowSticker] = useState(false);
  const [showHelp, setShowHelp] = useState(false);
  const [showFile, setShowFile] = useState(false);
  const account = useContext(AccountContext);
  if (!props.chat) {
    return null;
  }
  const send = (alterText = null) => {
    if (!alterText && text.length < 1) {
      alert("Пустое сообщение");
      return;
    }
    axios
      .post(props.igc ? "/message/group/send" : "/message/private/send", {
        to: props.chat.id,
        text: alterText || text,
      })
      .then(() => {
        setText("");
        props.send();
      })
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  };
  const handleKeypress = (e) => {
    if (e.shiftKey) {
      return;
    }
    if (e.keyCode === 13 || e.charCode === 13) {
      send();
    }
  };
  const smile = (value) => {
    setText(text + value);
  };
  // const uploadFile = (event) => {
  //   if (!event.target.files) {
  //     return;
  //   }
  //   const file = event.target.files[0];
  //   let q = new URLSearchParams();
  //   q.set("name", file.name);
  //   q.set("type", file.type);
  //   q.set("to", "" + props.chat.id);
  //   q.set("igc", "" + props.igc);
  //   axios
  //     .post("/message/file?" + q.toString(), file, {
  //       headers: {
  //         "Content-Type": file.type,
  //       },
  //     })
  //     .catch((err) =>
  //       alert(err.response.statusText + ":\n" + err.response.data)
  //     );
  // };
  return (
    <div id="chat-message">
      <div id="chat-input">
        <label
          className="action"
          title="Отправить файл"
          onClick={() => setShowFile(true)}
        >
          <img src="/file.png" alt="file" />
          {/* <input
            type="file"
            onChange={uploadFile}
            style={{ display: "none" }}
          /> */}
        </label>
        {showFile ? (
          <FileUpload
            close={() => setShowFile(false)}
            id={props.chat.id}
            igc={props.igc}
          />
        ) : null}
        <label
          className="action"
          onClick={() => setShowSticker(true)}
          title="Стикер/Смайл"
        >
          <img src="/sticker.png" alt="file" />
        </label>
        {showSticker ? (
          <SelectSticker
            close={() => setShowSticker(false)}
            send={send}
            smile={smile}
            account={account}
          />
        ) : null}
        <textarea
          placeholder="Сообщение"
          value={text}
          onChange={(e) => setText(e.target.value)}
          onKeyPress={handleKeypress}
          rows={1}
          className="message"
        />
        {showHelp ? <Help close={() => setShowHelp(false)} /> : null}
        <label
          style={{ marginRight: "0px", marginLeft: "8px" }}
          className="action"
          onClick={() => setShowHelp(true)}
          title={"Помощь"}
        >
          <img src="/help.png" alt="help" />
        </label>
        <button
          onClick={() => send()}
          className="action"
          title="Отправить сообщение"
        >
          <img src="/send.png" alt="send" />
        </button>
      </div>
    </div>
  );
}
