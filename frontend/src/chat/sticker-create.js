import axios from "axios";
import { useState } from "react";
import { Fullscreen } from "./service";

export function CreateSticker(props) {
  const [text, setText] = useState("");
  const [file, setFile] = useState(null);
  const [privt, setPrivt] = useState(true);
  const uploadFile = (event) => {
    if (!event.target.files) {
      return;
    }
    const f = event.target.files[0];
    setFile(f);
    setText(f.name.split(".")[0]);
  };
  const send = () => {
    try {
      let q = new URLSearchParams();
      q.set("name", file.name);
      q.set("type", file.type);
      q.set("sticker", text);
      q.set("private", privt);
      axios
        .post("/sticker/create?" + q.toString(), file, {
          headers: {
            "Content-Type": file.type,
          },
        })
        .then(() => props.close())
        .catch((err) =>
          alert(err.response.statusText + ":\n" + err.response.data)
        );
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <Fullscreen id="free">
      <b>Загрузка стикера</b>
      <br />
      <input type="file" onChange={uploadFile} accept="image/*" />
      <br />
      <label>Только 0-9, a-z, A-Z, _</label>
      <br />
      <input
        placeholder="Название"
        value={text}
        onChange={(e) => setText(e.target.value)}
      />
      <br />
      <label>
        Его название <i>:{text}:</i>
      </label>
      <br />
      <label>
        <input
          type="checkbox"
          checked={privt}
          onChange={(e) => setPrivt(e.target.checked)}
        />
        Приватный стикер
      </label>
      <br />
      <button onClick={send} disabled={!text.match(/^[0-9a-zA-Z_]+$/)}>
        Добавить
      </button>
      <button onClick={props.close}>Отмена</button>
    </Fullscreen>
  );
}
