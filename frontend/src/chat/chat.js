import axios from "axios";
import { Component, useContext } from "react";
import { AccountContext } from "../context";
import { ChatHead } from "./chat-head";
import { ChatInput } from "./chat-input";
import { ChatText } from "./chat-text";
import {
  anton,
  blackMix,
  blueGreenMix,
  fullBlack,
  skyBlue,
} from "./color-scheme";
import { Contact } from "./contact";
import { Menu } from "./menu";
import { offline, userFullname } from "./service";

export class ChatPage extends Component {
  constructor(props) {
    super(props);
    const enableAudio = JSON.parse(localStorage.getItem("enable-audio"));
    const volume = JSON.parse(localStorage.getItem("volume"));
    const theme = JSON.parse(localStorage.getItem("theme"));
    const backgroundImg = JSON.parse(localStorage.getItem("background-img"));
    this.state = {
      contacts: [],
      groups: [],
      messages: [],
      selectedChat: null,
      privateMessageCount: {},
      groupMessageCount: {},
      allMessageCount: 0,
      isGroupChat: false,
      enableAudio: enableAudio === null ? true : enableAudio,
      volume: volume === null ? 1 : volume,
      theme: theme === null ? "skyBlue" : theme,
      backgroundImg: backgroundImg === null ? "" : backgroundImg,
    };
    this.timerMCID = null;
    this.timerContactsID = null;
  }
  setSelectedChat = (value) => {
    this.setState({ selectedChat: value }, () => this.getMessages(true));
  };
  componentDidMount() {
    this.getMessageCount();
    this.getContacts();
    this.timerMCID = setInterval(() => this.getMessageCount(), 1000);
    this.timerContactsID = setInterval(() => this.getContacts(), 5000);
  }
  componentWillUnmount() {
    clearInterval(this.timerMCID);
    clearInterval(this.timerContactsID);
  }
  titleTicker = () => {
    let favicon = document.getElementById("favicon");
    if (favicon.href.indexOf("sheet.png") > -1) {
      favicon.href = "sheet_red.png";
    } else {
      favicon.href = "sheet.png";
    }
  };
  getMessageCount() {
    if (document.hidden) {
      this.setState({ selectedChat: null });
    }
    let loads = [
      axios.post("/message/private/count"),
      axios.post("/message/group/count"),
    ];
    Promise.all(loads)
      .then(([pmc, gmc]) => {
        let amc =
          Object.values(pmc.data).reduce((all, v) => all + v, 0) +
          Object.values(gmc.data).reduce((all, v) => all + v, 0);
        if (this.state.enableAudio && amc > this.state.allMessageCount) {
          let audio = new Audio("alert.mp3");
          audio.volume = this.state.volume;
          audio.play();
        }
        if (amc > 0) {
          document.title = "+" + amc + " новых сообщений";
          // document.getElementById("favicon").href = "sheet.gif";
          this.titleTicker();
        } else {
          document.title = "Чат сервер";
          document.getElementById("favicon").href = "sheet.png";
        }
        this.setState(
          {
            privateMessageCount: pmc.data,
            groupMessageCount: gmc.data,
            allMessageCount: amc,
          },
          () => this.getMessages()
        );
      })
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  }
  getMessages(force = false) {
    const cnt = this.state.selectedChat;
    if (!cnt) {
      return;
    }
    const id =
      (this.state.isGroupChat
        ? this.state.groupMessageCount[cnt.id]
        : this.state.privateMessageCount[cnt.id]) || 0;
    const updURL = this.state.isGroupChat
      ? "/message/group/update"
      : "/message/private/update";
    const allURL = this.state.isGroupChat
      ? "/message/group/all"
      : "/message/private/all";
    if (force || id) {
      if (!document.hidden) {
        axios
          .post(updURL, JSON.stringify(cnt.id))
          .catch((err) =>
            alert(err.response.statusText + ":\n" + err.response.data)
          );
      }
      axios
        .post(allURL, JSON.stringify(cnt.id))
        .then((data) => this.setState({ messages: data.data }))
        .catch((err) =>
          alert(err.response.statusText + ":\n" + err.response.data)
        );
    }
  }
  getContacts = () => {
    const cnt = this.state.selectedChat;
    axios
      .post("/account/list")
      .then((data) => {
        const contacts = data.data.sort((a, b) =>
          userFullname(a) > userFullname(b)
            ? 1
            : userFullname(a) < userFullname(b)
            ? -1
            : 0
        );
        if (cnt && !this.state.isGroupChat) {
          let exists = false;
          contacts.forEach((e) => {
            if (e.id === cnt.id) {
              exists = true;
              this.setSelectedChat(e);
            }
          });
          if (!exists) {
            this.setSelectedChat(null);
          }
        }
        this.setState({ contacts: contacts });
      })
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
    axios
      .post("/group/list")
      .then((data) => {
        const groups = data.data.sort((a, b) =>
          a.name > b.name ? 1 : a.name < b.name ? -1 : 0
        );
        if (cnt && this.state.isGroupChat) {
          let exists = false;
          groups.forEach((e) => {
            if (e.id === cnt.id) {
              exists = true;
              this.setSelectedChat(e);
            }
          });
          if (!exists) {
            this.setSelectedChat(null);
          }
        }
        this.setState({ groups: groups });
      })
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  };
  sendMessage = () => {
    this.getMessages(true);
  };
  switchIGC = (value) => {
    this.setState({ isGroupChat: value });
  };
  switchAudio = (value) => {
    this.setState({ enableAudio: value });
    localStorage.setItem("enable-audio", JSON.stringify(value));
  };
  setVolume = (event) => {
    const value = event.target.value;
    localStorage.setItem("volume", JSON.stringify(value));
    this.setState({ volume: value });
  };
  setTheme = (event) => {
    const value = event.target.value;
    localStorage.setItem("theme", JSON.stringify(value));
    this.setState({ theme: value });
  };
  setBackgroundImg = (event) => {
    const value = event.target.value;
    localStorage.setItem("background-img", JSON.stringify(value));
    this.setState({ backgroundImg: value });
  };
  render() {
    const getTheme = () => {
      switch (this.state.theme) {
        case "skyBlue":
          return skyBlue;
        case "blueGreenMix":
          return blueGreenMix;
        case "fullBlack":
          return fullBlack;
        case "anton":
          return anton;
        case "blackMix":
          return blackMix;
        default:
          return skyBlue;
      }
    };
    return (
      <>
        <style>
          {getTheme()}
          {`:root {--background-image: url("` +
            this.state.backgroundImg +
            `");}`}
        </style>
        <Menu
          switchAudio={this.switchAudio}
          audio={this.state.enableAudio}
          volume={this.state.volume}
          setVolume={this.setVolume}
        />
        <Contact
          onChange={this.setSelectedChat}
          pmc={this.state.privateMessageCount}
          gmc={this.state.groupMessageCount}
          chat={this.state.selectedChat}
          igc={this.state.isGroupChat}
          switchIGC={this.switchIGC}
          groups={this.state.groups}
          contacts={this.state.contacts}
          updateContact={this.getContacts}
        />
        <ChatFrame
          chat={this.state.selectedChat}
          pmc={this.state.privateMessageCount}
          messages={this.state.messages}
          igc={this.state.isGroupChat}
          groups={this.state.groups}
          contacts={this.state.contacts}
          theme={this.state.theme}
          setTheme={this.setTheme}
          backgroundImg={this.state.backgroundImg}
          setBackgroundImg={this.setBackgroundImg}
          updateContact={this.getContacts}
        />
        <ChatInput
          chat={this.state.selectedChat}
          send={this.sendMessage}
          igc={this.state.isGroupChat}
        />
      </>
    );
  }
}

function ChatFrame(props) {
  const cnt = props.chat;
  const account = useContext(AccountContext);
  const myid = account.accountInfo.id;
  if (!cnt) {
    return (
      <div id="chat-text">
        <table>
          <tbody>
            <tr>
              <td>
                <i>Ваш токен:</i>
              </td>
              <td>
                <details>
                  <summary>показать</summary>
                  <b>{account.accountInfo.token}</b>
                </details>
              </td>
            </tr>
            <tr>
              <td>
                <i> ФИО:</i>
              </td>
              <td>
                <b>{userFullname(account.accountInfo)}</b>
              </td>
            </tr>
            <tr>
              <td>
                <i>Он-лайн:</i>
              </td>
              <td>
                <b>
                  {props.contacts.reduce(
                    (all, v) => all + (offline(v) ? 0 : 1),
                    0
                  )}
                  {" из "}
                  {props.contacts.length}
                </b>
              </td>
            </tr>
            <tr>
              <td>
                <i>Доступных групп:</i>
              </td>
              <td>
                <b>{props.groups.length}</b>
              </td>
            </tr>
            <tr>
              <td>
                <i>Выбраная тема</i>
              </td>
              <td>
                <select
                  onChange={props.setTheme}
                  value={props.theme}
                  style={{ width: "100%" }}
                >
                  <option value="" hidden={true}>
                    Выбрать
                  </option>
                  <option value="skyBlue">Небесно-голубой</option>
                  <option value="blueGreenMix">Зелено-голубой микс</option>
                  <option value="fullBlack">Чисто черный</option>
                  <option value="anton">Тема Антона</option>
                  <option value="blackMix">Черный микс</option>
                </select>
              </td>
            </tr>
            <tr>
              <td>
                <i>Фоновое изображение:</i>
              </td>
              <td>
                <input
                  value={props.backgroundImg}
                  onChange={props.setBackgroundImg}
                  placeholder="Вставь ссылку на картинку"
                  style={{ width: "100%" }}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
  return (
    <>
      <ChatHead
        chat={props.chat}
        igc={props.igc}
        myid={myid}
        updateContact={props.updateContact}
      />
      <ChatText
        chat={cnt}
        myid={myid}
        pmc={props.pmc[cnt.id] || 0}
        messages={props.messages}
        igc={props.igc}
      />
    </>
  );
}
