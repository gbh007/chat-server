import axios from "axios";
import { useState } from "react";
import { Fullscreen } from "./service";

export function SetAvatar(props) {
  const [file, setFile] = useState(null);
  const uploadFile = (event) => {
    if (!event.target.files) {
      return;
    }
    const f = event.target.files[0];
    setFile(f);
  };
  const send = () => {
    try {
      let q = new URLSearchParams();
      q.set("name", file.name);
      q.set("type", file.type);
      axios
        .post("/avatar/set?" + q.toString(), file, {
          headers: {
            "Content-Type": file.type,
          },
        })
        .then(() => props.close())
        .catch((err) =>
          alert(err.response.statusText + ":\n" + err.response.data)
        );
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <Fullscreen id="free">
      <b>Смена аватара</b>
      <br />
      <input type="file" onChange={uploadFile} accept="image/*" />
      <br />
      <button onClick={send}>Установить</button>
      <button onClick={props.close}>Отмена</button>
    </Fullscreen>
  );
}
