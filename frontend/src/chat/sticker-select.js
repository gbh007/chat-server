import axios from "axios";
import { Component } from "react";
import { Fullscreen } from "./service";

export class SelectSticker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stickers: [],
      is_smile: true,
      all: false,
    };
  }
  componentDidMount() {
    axios
      .post("/sticker/list")
      .then((data) => this.setState({ stickers: data.data }))
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  }
  send = (name) => {
    if (this.state.is_smile) {
      this.props.smile(":" + name + ":");
    } else {
      this.props.send("sticker:" + name);
    }
    this.props.close();
  };
  render() {
    const id = this.props.account.accountInfo.id;
    const isAdmin = this.props.account.accountInfo.admin;
    // const show = this.state.all;
    return (
      <Fullscreen>
        <div
          style={{ maxWidth: "80vw", maxHeight: "80vh", overflow: "scroll" }}
          id="free"
        >
          <div
            style={{
              position: "sticky",
              top: "0px",
              display: isAdmin ? "block" : "none",
              background: "var(--background-fullscreen)",
            }}
          >
            <label>
              <input
                type="radio"
                checked={!this.state.all}
                onChange={() => this.setState({ all: false })}
              />
              Только доступные
            </label>
            <label>
              <input
                type="radio"
                checked={this.state.all}
                onChange={() => this.setState({ all: true })}
              />
              Все стикеры
            </label>
          </div>
          {this.state.stickers.map((sticker) =>
            this.state.all || !sticker.private || sticker.uploader_id === id ? (
              <img
                key={sticker.file}
                src={"/sticker/get?name=" + sticker.sticker_name}
                style={{ height: "5em" }}
                alt={sticker.sticker_name}
                title={sticker.sticker_name}
                onClick={() => {
                  this.send(sticker.sticker_name);
                }}
              />
            ) : null
          )}
          <div
            style={{
              position: "sticky",
              bottom: "0px",
              display: "block",
              background: "var(--background-fullscreen)",
            }}
          >
            <button onClick={() => this.props.close()}>Отмена</button>
            <label>
              <input
                type="radio"
                checked={this.state.is_smile}
                onChange={() => this.setState({ is_smile: true })}
              />
              Вставить как смайл
            </label>
            <label>
              <input
                type="radio"
                checked={!this.state.is_smile}
                onChange={() => this.setState({ is_smile: false })}
              />
              Отправить как стикер
            </label>
          </div>
        </div>
      </Fullscreen>
    );
  }
}
