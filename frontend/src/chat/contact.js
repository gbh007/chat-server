import { Component } from "react";
import { CreateGroup } from "./group-create";
import { offline, userShortname } from "./service";

export class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showGroupCreate: false,
    };
  }
  render() {
    return (
      <div id="contact">
        <details open={true}>
          <summary>Группы</summary>
          <table>
            <colgroup rules="groups">
              <col width="5%" />
              <col width="90%" />
              <col width="5%" />
            </colgroup>
            <tbody>
              <tr style={{ textAlign: "center" }}>
                <td colSpan={3}>
                  <button
                    onClick={() => {
                      this.props.switchIGC(true);
                      this.props.onChange(null);
                    }}
                  >
                    Главная страница
                  </button>
                </td>
              </tr>
              {this.props.groups.map((cnt, ind) => (
                <tr
                  key={ind}
                  onClick={() => {
                    this.props.switchIGC(true);
                    this.props.onChange(cnt);
                  }}
                  status={
                    this.props.igc &&
                    this.props.chat &&
                    cnt.id === this.props.chat.id
                      ? "active"
                      : "group"
                  }
                >
                  <td>
                    <img src="/group.png" alt="user" />
                  </td>
                  <td>
                    <label>{cnt.name}</label>
                  </td>
                  <td>
                    {this.props.gmc && this.props.gmc[cnt.id] ? (
                      <span className="mc"> +{this.props.gmc[cnt.id]}</span>
                    ) : null}
                  </td>
                </tr>
              ))}
              <tr style={{ textAlign: "center" }}>
                <td colSpan={3}>
                  <button
                    onClick={() => this.setState({ showGroupCreate: true })}
                  >
                    + Новая группа
                  </button>
                  {this.state.showGroupCreate ? (
                    <CreateGroup
                      updateContact={this.props.updateContact}
                      close={() => this.setState({ showGroupCreate: false })}
                    />
                  ) : null}
                </td>
              </tr>
            </tbody>
          </table>
        </details>
        <details open={true}>
          <summary>Пользователи</summary>
          <table>
            <colgroup rules="groups">
              <col width="5%" />
              <col width="90%" />
              <col width="5%" />
            </colgroup>
            <tbody>
              <tr style={{ textAlign: "center" }}>
                <td colSpan={3}>
                  <button
                    onClick={() => {
                      this.props.switchIGC(false);
                      this.props.onChange(null);
                    }}
                  >
                    Главная страница
                  </button>
                </td>
              </tr>
              {this.props.contacts.map((cnt, ind) => (
                <tr
                  key={ind}
                  onClick={() => {
                    this.props.switchIGC(false);
                    this.props.onChange(cnt);
                  }}
                  status={
                    !this.props.igc &&
                    this.props.chat &&
                    cnt.id === this.props.chat.id
                      ? "active"
                      : offline(cnt)
                      ? "offline"
                      : "online"
                  }
                >
                  <td>
                    <img
                      src={
                        cnt.avatar
                          ? "/avatar/get?name=" + cnt.avatar
                          : "/user.png"
                      }
                      alt="user"
                    />
                  </td>
                  <td>
                    <span className="name">
                      <label>{userShortname(cnt)}</label>
                    </span>
                  </td>
                  <td>
                    {this.props.pmc && this.props.pmc[cnt.id] ? (
                      <span className="mc"> +{this.props.pmc[cnt.id]}</span>
                    ) : null}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </details>
      </div>
    );
  }
}
