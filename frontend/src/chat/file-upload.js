import axios from "axios";
import { useState } from "react";
import { Fullscreen } from "./service";

export function FileUpload(props) {
  const [file, setFile] = useState(null);
  const uploadFile = (event) => {
    if (!event.target.files) {
      return;
    }
    const f = event.target.files[0];
    setFile(f);
  };
  const send = () => {
    try {
      let q = new URLSearchParams();
      q.set("name", file.name);
      q.set("type", file.type);
      q.set("to", "" + props.id);
      q.set("igc", "" + props.igc);
      axios
        .post("/message/file?" + q.toString(), file, {
          headers: {
            "Content-Type": file.type,
          },
        })
        .then(() => props.close())
        .catch((err) =>
          alert(err.response.statusText + ":\n" + err.response.data)
        );
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <Fullscreen id="free">
      <b>Отправка файла</b>
      <br />
      <input type="file" onChange={uploadFile} />
      <br />
      <sup>
        максимальный размер файла <b>10Мб</b>
      </sup>
      <br />
      <span style={{ color: "var(--color-alarm)" }}>
        Внимание! Файл будет доступен для скачивания только 3 месяца!
      </span>
      <br />
      <button onClick={send}>Отправить</button>
      <button onClick={props.close}>Отмена</button>
    </Fullscreen>
  );
}
