import axios from "axios";
import { useState } from "react";
import { offline, userFullname } from "./service";
import { UsersIn } from "./user-in-group";

export function ChatHead(props) {
  const myid = props.myid;
  const cnt = props.chat;
  const [showUsers, setShowUsers] = useState(false);
  const isOwner = props.igc && cnt.owner_id === myid;
  const deleteGroup = () => {
    const ok = window.confirm(
      "Вы действительно хотите удалить группу: " + cnt.name + "?"
    );
    if (!ok) {
      return;
    }
    axios
      .post("/group/delete", JSON.stringify(cnt.id))
      .then(() => props.updateContact())
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  };
  const leaveGroup = () => {
    const ok = window.confirm(
      "Вы действительно хотите покинуть группу: " + cnt.name + "?"
    );
    if (!ok) {
      return;
    }
    axios
      .post("/group/leave", JSON.stringify(cnt.id))
      .then(() => props.updateContact())
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  };
  const renameGroup = () => {
    const name = window.prompt("Введите новое название группы");
    if (name === null) {
      return;
    }
    if (name.length < 1 || name.length > 20) {
      alert("Неправильное название группы");
      return;
    }
    axios
      .post("/group/rename", { name: name, id: cnt.id })
      .then(() => props.updateContact())
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  };
  return (
    <div id="chat-head">
      {props.igc ? (
        <>
          <h1>{cnt.name}</h1>
          <i>{cnt.public ? " (Общедоступная)" : " (Приватная)"}</i>
        </>
      ) : (
        <>
          <h1 style={{ display: "inline-block" }}>{userFullname(cnt)}</h1>
          <span
            style={{
              background: offline(cnt)
                ? "var(--color-head-offline)"
                : "var(--color-head-online)",
              display: "inline-block",
              width: "20px",
              height: "20px",
              borderRadius: "10px",
              marginLeft: "10px",
            }}
          ></span>
        </>
      )}
      {props.igc ? (
        <button
          style={{ marginLeft: "10px" }}
          onClick={() => setShowUsers(true)}
        >
          Список пользователей
        </button>
      ) : null}
      {isOwner ? (
        <>
          <button style={{ marginLeft: "10px" }} onClick={renameGroup}>
            Переименовать группу
          </button>
          <button
            style={{ marginLeft: "10px", color: "var(--color-alarm)" }}
            onClick={deleteGroup}
          >
            Удалить группу
          </button>
        </>
      ) : props.igc ? (
        <button
          style={{ marginLeft: "10px", color: "var(--color-alarm)" }}
          onClick={leaveGroup}
        >
          Покинуть группу
        </button>
      ) : null}
      {showUsers ? (
        <UsersIn
          close={() => setShowUsers(false)}
          id={cnt.id}
          owner={isOwner}
          myid={myid}
        />
      ) : null}
    </div>
  );
}
