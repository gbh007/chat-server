export function Fullscreen(props) {
  return (
    <div id="fullscreen" onClick={props.onClick}>
      <div style={props.style} id={props.id}>
        {props.children}
      </div>
    </div>
  );
}

export function offline(user) {
  return new Date() - new Date(user.last_action) > 1000 * 60 * 5;
}
export function userFullname(user) {
  return user.lastname + " " + user.firstname + " " + user.patronimyc;
}

export function userShortname(user) {
  return (
    user.lastname +
    (user.firstname[0] ? " " + user.firstname[0] + "." : "") +
    (user.patronimyc[0] ? " " + user.patronimyc[0] + "." : "")
  );
}

function getRandomColor() {
  let letters = "0123456789ABCDEF".split("");
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

let colors = {};

export function GetColor(value) {
  if (colors[value]) {
    return colors[value];
  }
  const color = getRandomColor();
  colors[value] = color;
  return color;
}
