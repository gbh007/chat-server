import axios from "axios";
import { useContext, useState } from "react";
import { AccountContext } from "../context";
import { Fullscreen } from "./service";

export function RenameAccount(props) {
  const account = useContext(AccountContext);
  const [lastname, setLastname] = useState(account.accountInfo.lastname);
  const [firstname, setFirstname] = useState(account.accountInfo.firstname);
  const [patronimyc, setPatronimyc] = useState(account.accountInfo.patronimyc);
  const [color, setColor] = useState(account.accountInfo.color);
  const reg = () => {
    axios
      .post("/account/rename", {
        lastname: lastname,
        firstname: firstname,
        patronimyc: patronimyc,
        color: color,
      })
      .then(() => {
        account.update();
        alert("Успешно");
        props.close();
      })
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  };
  return (
    <Fullscreen id="free">
      <b>Смена ФИО</b>
      <br />
      <input
        placeholder="Фамилия"
        value={lastname}
        onChange={(e) => setLastname(e.target.value)}
      />
      <br />
      <input
        placeholder="Имя"
        value={firstname}
        onChange={(e) => setFirstname(e.target.value)}
      />
      <br />
      <input
        placeholder="Отчество"
        value={patronimyc}
        onChange={(e) => setPatronimyc(e.target.value)}
      />
      <br />
      Цвет:
      <input
        type="color"
        value={color}
        onChange={(e) => setColor(e.target.value)}
      />
      <br />
      <span>Ваш токен: {account.accountInfo.token}</span>
      <br />
      <button onClick={reg}>Подтвердить</button>
      <button onClick={props.close}>Отмена</button>
    </Fullscreen>
  );
}
