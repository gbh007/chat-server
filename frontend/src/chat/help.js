import { Fullscreen } from "./service";

export function Help(props) {
  return (
    <Fullscreen>
      <table id="padded">
        <thead>
          <tr>
            <td>
              <b>Функция</b>
            </td>
            <td>
              <b>Пример</b>
            </td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Для выделения курсивом используте 3 символа "_" с начала и конца
              текста
            </td>
            <td>
              ___<i>текст образец</i>___
            </td>
          </tr>
          <tr>
            <td>
              Для выделения жирным используте 3 символа "*" с начала и конца
              текста
            </td>
            <td>
              ***<b>текст образец</b>***
            </td>
          </tr>
          <tr>
            <td>
              Для перечеркивания текста используте 3 символа "-" с начала и
              конца текста
            </td>
            <td>
              ---<s>текст образец</s>---
            </td>
          </tr>
          <tr>
            <td>
              Для добавление смайла в текст используте двоеточие до и после
              названия смайла
            </td>
            <td>
              <i>:smile:</i>
            </td>
          </tr>
          <tr>
            <td>
              Для отправки стикера наберите "sticker:" и укажите название
              стикера
            </td>
            <td>
              <i> sticker:name</i>
            </td>
          </tr>
        </tbody>
      </table>
      <button onClick={() => props.close()}>Закрыть</button>
    </Fullscreen>
  );
}
