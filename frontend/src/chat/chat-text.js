import axios from "axios";
import { Component, useState } from "react";
import { Fullscreen, userFullname, userShortname } from "./service";

export class ChatText extends Component {
  constructor(props) {
    super(props);
    const format = JSON.parse(localStorage.getItem("chat-format"));
    this.state = {
      scroll: false,
      replace: true,
      stickers: "",
      format: format
        ? format
        : {
            url: true,
            sticker: true,
            smile: true,
            file: true,
            img: true,
            text: true,
          },
    };
    this.scrollTimerID = null;
  }
  setFormat = (key, value) => {
    const format = { ...this.state.format, [key]: value };
    localStorage.setItem("chat-format", JSON.stringify(format));
    this.setState({ format: format });
  };
  setScroll = (value) => this.setState({ scroll: value }, this.scroll);
  setReplace = (event) => this.setState({ replace: event.target.checked });
  componentDidMount() {
    this.scroll();
    this.getStickerList();
    this.scrollTimerID = setInterval(this.scrollOnInterval, 100);
  }
  componentWillUnmount() {
    clearInterval(this.scrollTimerID);
  }
  componentDidUpdate(prevProps) {
    if (prevProps.messages !== this.props.messages) {
      this.scroll();
      this.getStickerList();
    }
  }
  getStickerList = () => {
    axios
      .post("/sticker/list")
      .then((data) =>
        this.setState({
          stickers: data.data.reduce(
            (p, v) => p + (p === "" ? "" : "|") + v.sticker_name,
            ""
          ),
        })
      )
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  };
  scroll = () => {
    if (this.state.scroll) {
      return;
    }
    let el = document.getElementById("chat-text");
    el.scrollTo(0, el.scrollHeight);
  };
  scrollOnInterval = () => {
    if (this.state.scroll) {
      return;
    }
    let el = document.getElementById("chat-text");
    if (el.scrollTop + el.clientHeight !== el.scrollHeight) {
      this.scroll();
    }
  };
  onWheel = (event) => {
    let el = document.getElementById("chat-text");
    if (event.deltaY > 0) {
      if (el.scrollTop + el.clientHeight === el.scrollHeight) {
        this.setScroll(false);
      }
      return;
    }
    if (el.clientHeight < el.scrollHeight) {
      this.setScroll(true);
    }
  };
  escapeHtml = (unsafe) => {
    return unsafe
      .replaceAll(/&/g, "&amp;")
      .replaceAll(/</g, "&lt;")
      .replaceAll(/>/g, "&gt;")
      .replaceAll(/"/g, "&quot;")
      .replaceAll(/'/g, "&#039;");
  };
  replaceURLWithHTMLLinks = (text) => {
    const exp =
      /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|])/gi;
    return text.replaceAll(exp, "<a href='$1' target=\"_blank\">$1</a>");
  };
  replaceSmiles = (text) => {
    const rs = ":(" + this.state.stickers + "):";
    const exp = new RegExp(rs, "gi");
    return text.replaceAll(
      exp,
      `<img src="/sticker/get?name=$1" style="height: 2em; vertical-align: middle" alt="$1" title="$1"/>`
    );
  };
  replaceFormattedText = (text) => {
    return text
      .replaceAll(/___(.+?)___/gi, `<i>$1</i>`)
      .replaceAll(/\*\*\*(.+?)\*\*\*/gi, `<b>$1</b>`)
      .replaceAll(/---(.+?)---/gi, `<s>$1</s>`);
  };
  textToHTML = (text) => {
    const format = this.state.format;
    let tmp = this.escapeHtml(text);
    if (format.url) {
      tmp = this.replaceURLWithHTMLLinks(tmp);
    }
    if (format.smile) {
      tmp = this.replaceSmiles(tmp);
    }
    if (format.text) {
      tmp = this.replaceFormattedText(tmp);
    }
    return {
      __html: tmp,
    };
  };
  isFile = (value) =>
    this.state.format.file ? value.match(/^file:([0-9a-z]+):(.+)$/) : null;
  isImg = (value) =>
    this.state.format.img
      ? value.match(/^file:([0-9a-z]+):(.+)\.(png|jpe?g|giff?|bmp|ico|tiff)$/i)
      : null;
  isSticker = (value) =>
    this.state.format.sticker ? value.match(/^sticker:([0-9a-zA-Z_]+)$/) : null;
  render() {
    const format = this.state.format;
    const replace = format.smile || format.url || format.text;
    const visible = [
      { code: "smile", name: "смайлы" },
      { code: "url", name: "ссылки" },
      { code: "file", name: "файлы" },
      { code: "img", name: "предпросмотр" },
      { code: "text", name: "текст" },
      { code: "sticker", name: "стикеры" },
    ];
    return (
      <div id="chat-text" onWheel={this.onWheel}>
        <div
          style={{
            position: "sticky",
            top: "0%",
            background: "var(--background)",
            borderBottom: "1px dotted black",
          }}
          id="free"
        >
          <span
            style={{
              borderRight: "3px solid black",
              paddingRight: "5px",
              paddingLeft: "5px",
            }}
          >
            Отображать
          </span>
          {visible.map((v, ind) => (
            <label key={ind}>
              <input
                type="checkbox"
                checked={this.state.format[v.code]}
                onChange={(e) => this.setFormat(v.code, e.target.checked)}
              />
              {v.name}
            </label>
          ))}
        </div>
        <table>
          <colgroup rules="groups">
            <col width="5%" />
            <col width="90%" />
            <col width="5%" />
          </colgroup>
          <tbody>
            {this.props.messages.map((mess) => (
              <tr key={mess.id}>
                {mess.sender_id !== this.props.myid ? (
                  <td id="name">
                    <img
                      src={
                        mess.sender.avatar
                          ? "/avatar/get?name=" + mess.sender.avatar
                          : "/user.png"
                      }
                      alt="Стикер"
                      title="Аватар"
                      style={{
                        maxWidth: "3em",
                        maxHeight: "3em",
                        verticalAlign: "middle",
                      }}
                    />
                    <div
                      style={{
                        display: "inline-block",
                        marginLeft: "5px",
                        verticalAlign: "middle",
                      }}
                    >
                      <span
                        title={userFullname(mess.sender)}
                        style={{ color: mess.sender.color }}
                      >
                        {userShortname(mess.sender)}
                      </span>
                      <br />
                      <i className="time">
                        {new Date(mess.date).toLocaleString()}
                      </i>
                    </div>
                  </td>
                ) : null}
                <td
                  id="text"
                  className={mess.sender_id === this.props.myid ? "me" : ""}
                  colSpan={2}
                >
                  <div>
                    {this.isImg(mess.text) ? (
                      <ShowSticker
                        name={this.isImg(mess.text)[1]}
                        isimg={true}
                        alt={mess.text}
                      />
                    ) : this.isFile(mess.text) ? (
                      <ShowFile file={this.isFile(mess.text)} />
                    ) : this.isSticker(mess.text) ? (
                      <ShowSticker name={this.isSticker(mess.text)[1]} />
                    ) : replace ? (
                      <pre
                        dangerouslySetInnerHTML={this.textToHTML(mess.text)}
                      ></pre>
                    ) : (
                      <pre>{mess.text}</pre>
                    )}
                  </div>
                </td>
                {mess.sender_id === this.props.myid ? (
                  <td id="me">
                    <div
                      style={{
                        display: "inline-block",
                        marginRight: "5px",
                        verticalAlign: "middle",
                      }}
                    >
                      <span
                        title={userFullname(mess.sender)}
                        style={{ color: mess.sender.color }}
                      >
                        {userShortname(mess.sender)}
                      </span>
                      <br />
                      <i className="time">
                        {new Date(mess.date).toLocaleString()}
                      </i>
                    </div>
                    <img
                      src={
                        mess.sender.avatar
                          ? "/avatar/get?name=" + mess.sender.avatar
                          : "/user.png"
                      }
                      alt="Стикер"
                      title="Аватар"
                      style={{
                        maxWidth: "3em",
                        maxHeight: "3em",
                        verticalAlign: "middle",
                      }}
                    />
                  </td>
                ) : null}
              </tr>
            ))}
          </tbody>
        </table>
        {this.state.scroll === true ? (
          <span
            className="to-new"
            style={{ position: "sticky", bottom: "0%" }}
            onClick={() => this.setScroll(false)}
          >
            <label>назад к новым сообщениям</label>
          </span>
        ) : null}
      </div>
    );
  }
}

function ShowFile(props) {
  const filename = props.file[1];
  const name = props.file[2];
  return (
    <a href={"/file?name=" + filename} download={name}>
      <img
        src="/file.png"
        alt="file"
        style={{ width: "1em", verticalAlign: "middle" }}
      />
      {name}
    </a>
  );
}

function ShowSticker(props) {
  const [fs, setFS] = useState(false);
  const url = (props.isimg ? "/file?name=" : "/sticker/get?name=") + props.name;
  const alt = props.alt || "Стикер";
  return fs ? (
    <Fullscreen onClick={() => setFS(false)}>
      <img
        src={url}
        style={{ maxHeight: "90vh", maxWidth: "90vw" }}
        alt={alt}
        title={props.name}
        onClick={() => setFS(false)}
      />
    </Fullscreen>
  ) : (
    <img
      src={url}
      style={{ maxHeight: "10em", maxWidth: "50vw" }}
      alt={alt}
      title={props.name}
      onClick={() => setFS(true)}
    />
  );
}
