import axios from "axios";
import { Component } from "react";
import { Fullscreen, userFullname } from "./service";

export class UsersIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      usersAll: [],
      inIDs: [],
    };
  }
  componentDidMount() {
    this.getUsers();
  }
  getUsers() {
    const sort = (a, b) =>
      userFullname(a) > userFullname(b)
        ? 1
        : userFullname(a) < userFullname(b)
        ? -1
        : 0;
    Promise.all([
      axios.post("/group/user/list", JSON.stringify(this.props.id)),
      axios.post("/account/list"),
    ])
      .then(([uig, uall]) => {
        this.setState({
          users: uig.data.sort(sort),
          usersAll: uall.data.sort(sort),
          inIDs: uig.data.map((e) => e.id),
        });
      })
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  }
  addUser(id) {
    axios
      .post("/group/user/add", { group_id: this.props.id, user_id: id })
      .then(() => this.getUsers())
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  }
  delUser(id) {
    axios
      .post("/group/user/del", { group_id: this.props.id, user_id: id })
      .then(() => this.getUsers())
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  }
  render() {
    return (
      <Fullscreen>
        В группе
        <ol>
          {this.state.users.map((u, ind) => (
            <li key={ind}>
              {userFullname(u)}
              {this.props.owner && this.props.myid !== u.id ? (
                <button
                  style={{ color: "red" }}
                  onClick={() => this.delUser(u.id)}
                >
                  Удалить
                </button>
              ) : null}
            </li>
          ))}
        </ol>
        {this.props.owner ? (
          <>
            {"Все"}
            <ol>
              {this.state.usersAll.map((u, ind) =>
                this.state.inIDs.includes(u.id) ? null : (
                  <li key={ind}>
                    {userFullname(u)}
                    <button
                      style={{ color: "green" }}
                      onClick={() => this.addUser(u.id)}
                    >
                      Добавить
                    </button>
                  </li>
                )
              )}
            </ol>
          </>
        ) : null}
        <button onClick={this.props.close}>Закрыть</button>
      </Fullscreen>
    );
  }
}
