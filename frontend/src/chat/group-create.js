import axios from "axios";
import { useState } from "react";
import { Fullscreen } from "./service";

export function CreateGroup(props) {
  const [text, setText] = useState("");
  const [pub, setPublic] = useState(false);
  const send = () => {
    try {
      axios
        .post("/group/create", { name: text, public: pub })
        .then(() => {
          props.updateContact();
          props.close();
        })
        .catch((err) =>
          alert(err.response.statusText + ":\n" + err.response.data)
        );
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <Fullscreen id="free">
      <b>Создание группы</b>
      <br />
      <input
        placeholder="Название"
        value={text}
        onChange={(e) => setText(e.target.value)}
      />
      <br />
      <label style={{ fontWeight: "normal" }}>
        <input type="radio" checked={pub} onChange={(e) => setPublic(true)} />
        Публичная группа
      </label>
      <br />
      <label style={{ fontWeight: "normal" }}>
        <input type="radio" checked={!pub} onChange={(e) => setPublic(false)} />
        Приватная группа
      </label>
      <br />
      <button onClick={send}>Создать</button>
      <button onClick={props.close}>Отмена</button>
    </Fullscreen>
  );
}
