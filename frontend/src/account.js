import { Link } from "react-router-dom";
import axios from "axios";
import { useContext, useState } from "react";
import { AccountContext } from "./context";

export function LoginPage() {
  const [token, setToken] = useState("");
  const account = useContext(AccountContext);
  return (
    <div id="login">
      <h1>Авторизация</h1>
      <br />
      <input
        placeholder="Ключ доступа"
        value={token}
        onChange={(e) => setToken(e.target.value)}
      />
      <br />
      <button onClick={() => account.login(token)}>Войти</button>
      <br />
      <Link to="registration">
        <span>Регистрация</span>
      </Link>
    </div>
  );
}

export function RegistrationPage() {
  const [lastname, setLastname] = useState("");
  const [firstname, setFirstname] = useState("");
  const [patronimyc, setPatronimyc] = useState("");
  const account = useContext(AccountContext);
  const reg = () => {
    axios
      .post("/account/create", {
        lastname: lastname,
        firstname: firstname,
        patronimyc: patronimyc,
      })
      .then((data) => {
        alert(
          "Упешная регистрация ваш токен:\n" +
            data.data +
            "\nНЕ ЗАБУДТЕ ЕГО СОХРАНИТЬ (ЗАПИСАТЬ)!"
        );
        account.update();
      })
      .catch((err) =>
        alert(err.response.statusText + ":\n" + err.response.data)
      );
  };
  return (
    <div id="login">
      <h1>Регистрация</h1>
      <br />
      <input
        placeholder="Фамилия"
        value={lastname}
        onChange={(e) => setLastname(e.target.value)}
      />
      <br />
      <input
        placeholder="Имя"
        value={firstname}
        onChange={(e) => setFirstname(e.target.value)}
      />
      <br />
      <input
        placeholder="Отчество"
        value={patronimyc}
        onChange={(e) => setPatronimyc(e.target.value)}
      />
      <br />
      <button onClick={reg}>Зарегистрироваться</button>
    </div>
  );
}
