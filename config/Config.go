package config

import (
	"encoding/json"
	"log"
	"os"
	"path"

	"gitlab.com/gbh007/gojlog"
)

// StartupShortLog детальный лог загрузки
var StartupShortLog = gojlog.ShortLog{}

// startupShortLogName имя файла лога загрузки
const startupShortLogName = "last-startup.shlg"

// SaveStartupShortLog сохраняет последний лог запуска в файл
func SaveStartupShortLog() error {
	p := ""
	if _config.Log.File.Enable {
		p, _ = path.Split(_config.Log.File.Filename)
	}
	if p != "" {
		p = path.Join(p, startupShortLogName)
	} else {
		p = startupShortLogName
	}
	file, err := os.Create(p)
	defer file.Close()
	if err != nil {
		// выводим только в стандартный поток ошибок
		log.Println(err)
		return err
	}
	_, err = file.Write(StartupShortLog.Bytes())
	if err != nil {
		// выводим только в стандартный поток ошибок
		log.Println(err)
	}
	return err
}

// Config структура для хранения конфигурации приложения
type Config struct {
	Conn string           `json:"conn"`
	Log  gojlog.LogConfig `json:"log"`
}

var _config Config

// GetConfig возврашает объект конфигурации
func GetConfig() Config {
	return _config
}

// Load загружает конфиг
func Load(configPath string) error {
	file, err := os.Open(configPath)
	if err != nil {
		StartupShortLog.Critical("не могу открыть файл с конфигурацией ", configPath, err)
		return err
	}
	defer file.Close()
	d := json.NewDecoder(file)
	if err := d.Decode(&_config); err != nil {
		StartupShortLog.Critical("не могу загрузить конфигурацию ", configPath, err)
		return err
	}
	StartupShortLog.Info("конфигурация загружена ", configPath)
	gojlog.SetConfig(_config.Log)
	return nil
}
