package web

import (
	"app/db"
	"net/http"

	"gitlab.com/gbh007/gojlog"
)

// CreateGroup создает новую группу
func CreateGroup() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := struct {
			Name   string `json:"name"`
			Public bool   `json:"public"`
		}{}
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		acc := db.Group{
			Name:    req.Name,
			OwnerID: u.ID,
			Public:  req.Public,
		}
		id, err := db.InsertGroup(acc)
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		// добавляем пользователей в группу
		if req.Public {
			for _, user := range db.SelectUsers() {
				db.InsertUserInGroup(user.ID, id)
			}
		} else {
			db.InsertUserInGroup(u.ID, id)
		}
		SetResponse(r, struct{}{})
		gojlog.Infof("%s создал(а) группу #%d - %s", GetAccountInfo(r), id, req.Name)
	})
}

// AllGroups возвращает список всех групп
func AllGroups() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, _ := GetSUPOk(r)
		SetResponse(r, db.SelectGroups(u.ID))
	})
}

// AddUserInGroup добавляет пользователя в группу
func AddUserInGroup() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := struct {
			UserID  int `json:"user_id"`
			GroupID int `json:"group_id"`
		}{}
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		group, err := db.SelectGroupByID(req.GroupID)
		if err != nil {
			SetError(r, ErrProcessingData)
			return
		}
		// только создатель может редактировать группу
		if u.ID != group.OwnerID {
			SetError(r, ErrForbidden)
			return
		}
		err = db.InsertUserInGroup(req.UserID, req.GroupID)
		if err != nil {
			SetError(r, ErrCreateData)
		} else {
			SetResponse(r, struct{}{})
			gojlog.Infof("%s добавил(а) пользователя %d в группу %d", GetAccountInfo(r), req.UserID, req.GroupID)
		}
	})
}

// DeleteUserFromGroup удаляет пользователя из группы
func DeleteUserFromGroup() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := struct {
			UserID  int `json:"user_id"`
			GroupID int `json:"group_id"`
		}{}
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		// пользователь не может удалить сам себя
		if u.ID == req.UserID {
			SetError(r, ErrForbidden)
			return
		}
		group, err := db.SelectGroupByID(req.GroupID)
		if err != nil {
			SetError(r, ErrProcessingData)
			return
		}
		// только создатель может редактировать группу
		if u.ID != group.OwnerID {
			SetError(r, ErrForbidden)
			return
		}
		err = db.DeleteUserFromGroup(req.UserID, req.GroupID)
		if err != nil {
			SetError(r, ErrCreateData)
		} else {
			SetResponse(r, struct{}{})
			gojlog.Infof("%s удалил(а) пользователя %d из группы %d", GetAccountInfo(r), req.UserID, req.GroupID)
		}
	})
}

// AllUsers возвращает список всех пользователей в группе
func AllUsersInGroup() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req int
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		users := db.SelectUsersInGroup(req)
		for i := range users {
			users[i].Token = ""
			users[i].AES = ""
		}
		SetResponse(r, users)
	})
}

// RenameGroup переименовывает группу
func RenameGroup() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := struct {
			ID   int    `json:"id"`
			Name string `json:"name"`
		}{}
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		group, err := db.SelectGroupByID(req.ID)
		if err != nil {
			SetError(r, ErrProcessingData)
			return
		}
		// только создатель может редактировать группу
		if u.ID != group.OwnerID {
			SetError(r, ErrForbidden)
			return
		}
		err = db.UpdateGroupName(req.ID, req.Name)
		if err != nil {
			SetError(r, ErrUpdateData)
		} else {
			SetResponse(r, struct{}{})
			gojlog.Infof("%s переименовал(а) группу %d", GetAccountInfo(r), req.ID)
		}
	})
}

// DeleteGroup удаляет группу
func DeleteGroup() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req int
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		group, err := db.SelectGroupByID(req)
		if err != nil {
			SetError(r, ErrProcessingData)
			return
		}
		// только создатель может редактировать группу
		if u.ID != group.OwnerID {
			SetError(r, ErrForbidden)
			return
		}
		err = db.DeleteGroup(req)
		if err != nil {
			SetError(r, ErrDeleteData)
		} else {
			SetResponse(r, struct{}{})
			gojlog.Infof("%s удалил(а) группу %d", GetAccountInfo(r), req)
		}
	})
}

// LeaveGroup покидает группу
func LeaveGroup() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req int
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		group, err := db.SelectGroupByID(req)
		if err != nil {
			SetError(r, ErrProcessingData)
			return
		}
		// создатель не может покинуть группу
		if u.ID == group.OwnerID {
			SetError(r, ErrForbidden)
			return
		}
		err = db.DeleteUserFromGroup(u.ID, req)
		if err != nil {
			SetError(r, ErrDeleteData)
		} else {
			SetResponse(r, struct{}{})
			gojlog.Infof("%s покинул(а) группу %d", GetAccountInfo(r), req)
		}
	})
}
