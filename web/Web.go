package web

import (
	"net/http"
	"time"

	"gitlab.com/gbh007/gojlog"
)

// Run запускает веб сервер
func Run(addr, staticDir, filesDir string) <-chan struct{} {
	mux := http.NewServeMux()
	// обработчик статики
	mux.Handle("/", http.FileServer(http.Dir(staticDir)))
	// обработчики учеток
	AddHandler(mux, "/account/login", false, Login())
	AddHandler(mux, "/account/logout", false, Logout())
	AddHandler(mux, "/account/create", false, CreateAccount())
	AddHandler(mux, "/account/info", true, AccountInfo())
	AddHandler(mux, "/account/list", true, AllUsers())
	AddHandler(mux, "/account/rename", true, RenameAccount())
	// обработчики приватных сообщений
	AddHandler(mux, "/message/private/send", true, PrivateMessage())
	AddHandler(mux, "/message/private/all", true, PrivateAll())
	AddHandler(mux, "/message/private/count", true, PrivateCount())
	AddHandler(mux, "/message/private/update", true, PrivateUpdateReaded())
	// обработчики групповых сообщений
	AddHandler(mux, "/message/group/send", true, GroupMessage())
	AddHandler(mux, "/message/group/all", true, GroupAll())
	AddHandler(mux, "/message/group/count", true, GroupCount())
	AddHandler(mux, "/message/group/update", true, GroupUpdateReaded())
	// обработчики файлов
	AddHandler(mux, "/message/file", true, FileMessage(filesDir))
	AddFileHandler(mux, "/file", GetFile(filesDir))
	AddHandler(mux, "/sticker/create", true, AddSticker(filesDir))
	AddFileHandler(mux, "/sticker/get", GetSticker(filesDir))
	AddHandler(mux, "/sticker/list", true, GetStickerList())
	AddFileHandler(mux, "/avatar/get", GetAvatar(filesDir))
	AddHandler(mux, "/avatar/set", true, SetAvatar(filesDir))
	// обработчики групп
	AddHandler(mux, "/group/create", true, CreateGroup())
	AddHandler(mux, "/group/list", true, AllGroups())
	AddHandler(mux, "/group/user/list", true, AllUsersInGroup())
	AddHandler(mux, "/group/user/add", true, AddUserInGroup())
	AddHandler(mux, "/group/user/del", true, DeleteUserFromGroup())
	AddHandler(mux, "/group/rename", true, RenameGroup())
	AddHandler(mux, "/group/delete", true, DeleteGroup())
	AddHandler(mux, "/group/leave", true, LeaveGroup())
	// создание объекта сервера
	server := &http.Server{
		Addr:         addr,
		Handler:      mux,
		ReadTimeout:  1 * time.Minute,
		WriteTimeout: 1 * time.Minute,
		IdleTimeout:  1 * time.Minute,
	}
	done := make(chan struct{})
	go func() {
		if err := server.ListenAndServe(); err != nil {
			gojlog.Error(err)
		}
		close(done)
	}()
	return done
}
