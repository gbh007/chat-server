package web

import (
	"app/db"
	"io"
	"net/http"
	"os"
	"path"
	"strconv"
	"time"

	"gitlab.com/gbh007/gojlog"
)

// FileMessage обрабатывает файловое сообщение
func FileMessage(filesDir string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, _ := GetSUPOk(r)
		name := r.URL.Query().Get("name")
		mime := r.URL.Query().Get("type")
		lenSrt, err := strconv.Atoi(r.Header.Get("Content-Length"))
		if err != nil {
			SetError(r, ErrParseData)
			return
		}
		// 10Mb
		if lenSrt > 10*1024*1024 {
			SetError(r, ErrToLargeData)
			return
		}
		to, err := strconv.Atoi(r.URL.Query().Get("to"))
		if err != nil {
			SetError(r, ErrParseData)
			return
		}
		igc, err := strconv.ParseBool(r.URL.Query().Get("igc"))
		if err != nil {
			SetError(r, ErrParseData)
			return
		}
		fileinfo := db.File{
			Filename:   hash(name + time.Now().String()),
			Name:       name,
			Date:       time.Now(),
			UploaderID: u.ID,
			Mime:       mime,
			Private:    true,
		}
		// если файл существует то выходим
		if _, err := os.Stat(path.Join(filesDir, fileinfo.Filename)); err == nil {
			SetError(r, ErrProcessingData)
			return
		}
		f, err := os.Create(path.Join(filesDir, fileinfo.Filename))
		defer f.Close()
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		l, err := io.Copy(f, r.Body)
		defer r.Body.Close()
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		fileinfo.Size = l
		err = db.InsertFile(fileinfo)
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		text := "file:" + fileinfo.Filename + ":" + fileinfo.Name
		if igc {
			err = groupMessage(fileinfo.Date, text, u.ID, to)
			if err != nil {
				SetError(r, ErrCreateData)
				return
			}
		} else {
			err = privateMessage(fileinfo.Date, text, u.ID, to)
			if err != nil {
				SetError(r, ErrCreateData)
				return
			}
		}
		SetResponse(r, struct{}{})
		gojlog.Infof("%s загрузил(а) файл %s", GetAccountInfo(r), name)
	})
}

// GetFile возвращает файл по имени
func GetFile(filesDir string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		name := r.URL.Query().Get("name")
		fileInfo, err := db.SelectFileByFilename(name)
		if err != nil {
			SetError(r, ErrNotFound)
			return
		}
		f, err := os.Open(path.Join(filesDir, fileInfo.Filename))
		if err != nil {
			SetError(r, ErrNotFound)
			return
		}
		SetResponse(r, f)
		SetFilename(r, fileInfo.Name)
		SetFileType(r, fileInfo.Mime)
	})
}

// AddSticker добавляет стикер
func AddSticker(filesDir string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, _ := GetSUPOk(r)
		name := r.URL.Query().Get("name")
		mime := r.URL.Query().Get("type")
		sticker := r.URL.Query().Get("sticker")
		private, err := strconv.ParseBool(r.URL.Query().Get("private"))
		if err != nil {
			SetError(r, ErrParseData)
			return
		}
		lenSrt, err := strconv.Atoi(r.Header.Get("Content-Length"))
		if err != nil {
			SetError(r, ErrParseData)
			return
		}
		// 10Mb
		if lenSrt > 10*1024*1024 {
			SetError(r, ErrToLargeData)
			return
		}
		fileinfo := db.File{
			Filename:    hash(name + time.Now().String()),
			Name:        name,
			Date:        time.Now(),
			UploaderID:  u.ID,
			Mime:        mime,
			StickerName: sticker,
			Private:     private,
		}
		// если файл существует то выходим
		if _, err := os.Stat(path.Join(filesDir, fileinfo.Filename)); err == nil {
			SetError(r, ErrProcessingData)
			return
		}
		f, err := os.Create(path.Join(filesDir, fileinfo.Filename))
		defer f.Close()
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		l, err := io.Copy(f, r.Body)
		defer r.Body.Close()
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		fileinfo.Size = l
		err = db.InsertFile(fileinfo)
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		SetResponse(r, struct{}{})
		gojlog.Infof("%s загрузил(а) стикер %s", GetAccountInfo(r), sticker)
	})
}

// GetSticker возвращает стикер по названию
func GetSticker(filesDir string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		name := r.URL.Query().Get("name")
		fileInfo, err := db.SelectFileByStickerName(name)
		if err != nil {
			SetError(r, ErrNotFound)
			return
		}
		f, err := os.Open(path.Join(filesDir, fileInfo.Filename))
		if err != nil {
			SetError(r, ErrNotFound)
			return
		}
		SetResponse(r, f)
		SetFileType(r, fileInfo.Mime)
	})
}

// GetStickerList возвращает все стикеры
func GetStickerList() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		SetResponse(r, db.SelectStickers())
	})
}

// SetAvatar добавляет аватар
func SetAvatar(filesDir string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, _ := GetSUPOk(r)
		name := r.URL.Query().Get("name")
		mime := r.URL.Query().Get("type")
		lenSrt, err := strconv.Atoi(r.Header.Get("Content-Length"))
		if err != nil {
			SetError(r, ErrParseData)
			return
		}
		// 1Mb
		if lenSrt > 1*1024*1024 {
			SetError(r, ErrToLargeData)
			return
		}
		fileinfo := db.File{
			Filename:   hash(name + time.Now().String()),
			Name:       name,
			Date:       time.Now(),
			UploaderID: u.ID,
			Mime:       mime,
			Private:    true,
			IsAvatar:   true,
		}
		// если файл существует то выходим
		if _, err := os.Stat(path.Join(filesDir, fileinfo.Filename)); err == nil {
			SetError(r, ErrProcessingData)
			return
		}
		f, err := os.Create(path.Join(filesDir, fileinfo.Filename))
		defer f.Close()
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		l, err := io.Copy(f, r.Body)
		defer r.Body.Close()
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		fileinfo.Size = l
		err = db.InsertFile(fileinfo)
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		err = db.UpdateAvatar(u.ID, fileinfo.Filename)
		if err != nil {
			SetError(r, ErrUpdateData)
			return
		}
		SetResponse(r, struct{}{})
		gojlog.Infof("%s изменил(а) аватар", GetAccountInfo(r))
	})
}

// GetAvatar возвращает аватар пользователя
func GetAvatar(filesDir string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		name := r.URL.Query().Get("name")
		fileInfo, err := db.SelectFileByFilename(name)
		if err != nil {
			SetError(r, ErrNotFound)
			return
		}
		f, err := os.Open(path.Join(filesDir, fileInfo.Filename))
		if err != nil {
			SetError(r, ErrNotFound)
			return
		}
		SetResponse(r, f)
		SetFileType(r, fileInfo.Mime)
	})
}
