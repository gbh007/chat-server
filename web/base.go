package web

import (
	"app/db"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"strings"
)

// CoreTokenName имя печеньки с ключом сессии
const CoreTokenName = "chat-token"

// перменные ошибок
var (
	ErrNotAuth        = fmt.Errorf("Необходима авторизация")
	ErrForbidden      = fmt.Errorf("Нет прав доступа")
	ErrNotFound       = fmt.Errorf("Данные не найдены")
	ErrParseData      = fmt.Errorf("Некорректный формат данных")
	ErrDeleteData     = fmt.Errorf("Невозможно удалить данные")
	ErrCreateData     = fmt.Errorf("Невозможно создать данные")
	ErrAppendData     = fmt.Errorf("Невозможно добавить данные")
	ErrUpdateData     = fmt.Errorf("Невозможно изменить данные")
	ErrProcessingData = fmt.Errorf("Невозможно обработать данные")
	ErrToLargeData    = fmt.Errorf("Слишком большой объем данных")
)

// ParseJSON читает из тела запроса переданную структуру
func ParseJSON(r *http.Request, data interface{}) error {
	return json.NewDecoder(r.Body).Decode(&data)
}

// GetSessionToken получает токен из печеньки
func GetSessionToken(r *http.Request) (token string, err error) {
	cookie, err := r.Cookie(CoreTokenName)
	if err != nil {
		return
	}
	token = cookie.Value
	return
}

// CheckSession проверяет сессию
func CheckSession(r *http.Request) (u db.User, e error) {
	token, err := GetSessionToken(r)
	if err != nil {
		return
	}
	u, err = db.SelectUserByToken(token)
	return
}

// GetIP возвращает реальный ip пользователя
func GetIP(r *http.Request) string {
	if ip := r.Header.Get("X-REAL-IP"); ip != "" {
		return ip
	}
	if ips := r.Header.Get("X-FORWARDED-FOR"); ips != "" {
		for _, ip := range strings.Split(ips, ",") {
			return ip
		}
	}
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return ""
	}
	return ip
}

// GetSUPOk возвращает данные о пользователе
// используется только после прохождения запросом PermissionHandler (вносит данные в запрос)
// возвращает сессию, пользователя
func GetSUPOk(r *http.Request) (db.User, bool) {
	u, ok := r.Context().Value(ResponseUserKey).(db.User)
	return u, ok && u.ID > 0
}

// GetAccountInfo возвращает информацию об аккаунте с которого выполняется запрос
// формат выходной строки [USER_EMAIL-OR-USER_LOGIN/IP] или [unauthorized/IP]
func GetAccountInfo(r *http.Request) string {
	info := "["
	u, ok := GetSUPOk(r)
	if ok {
		info += u.Lastname
	} else {
		info += "unauthorized"
	}
	info += "/" + GetIP(r) + "]"
	return info
}
