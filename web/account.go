package web

import (
	"app/db"
	"crypto/md5"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/gbh007/gojlog"
)

func hash(s string) string { return fmt.Sprintf("%x", md5.Sum([]byte(s))) }

// CreateAccount создает новую учетную запись
func CreateAccount() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := struct {
			Lastname   string `json:"lastname"`
			Firstname  string `json:"firstname"`
			Patronimyc string `json:"patronimyc"`
		}{}
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		token := hash(req.Lastname + req.Firstname + time.Now().String())
		acc := db.User{
			Lastname:   req.Lastname,
			Firstname:  req.Firstname,
			Patronimyc: req.Patronimyc,
			Token:      token[:5],
			AES:        token,
			Color:      "#000000",
		}
		id, err := db.InsertUser(acc)
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		// добавляем пользователя в публичные гурппы
		for _, g := range db.SelectPublicGroups() {
			db.InsertUserInGroup(id, g.ID)
		}
		http.SetCookie(w, &http.Cookie{
			Name:     CoreTokenName,
			Value:    acc.Token,
			Path:     "/",
			HttpOnly: true,
		})
		gojlog.Infof("%s новая учетная запись", req.Lastname)
		SetResponse(r, acc.Token)
	})
}

// Login авторизирует учетную запись
func Login() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req string
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, err := db.SelectUserByToken(req)
		if err != nil {
			SetError(r, ErrNotFound)
		} else {
			http.SetCookie(w, &http.Cookie{
				Name:     CoreTokenName,
				Value:    req,
				Path:     "/",
				HttpOnly: true,
			})
			SetResponse(r, u)
			gojlog.Infof("%s авторизован(а)", u.Lastname)
		}
	})
}

// Login выходит из учетной записи
func Logout() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.SetCookie(w, &http.Cookie{
			Name:     CoreTokenName,
			Value:    "-",
			Path:     "/",
			HttpOnly: true,
		})
		SetResponse(r, struct{}{})
		gojlog.Infof("%s вышел(а)", GetAccountInfo(r))
	})
}

// AccountInfo возвращает данные учетной записи
func AccountInfo() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, ok := GetSUPOk(r)
		if !ok {
			SetError(r, ErrNotFound)
		} else {
			SetResponse(r, u)
		}
	})
}

// AllUsers возвращает список всех пользователей
func AllUsers() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		users := db.SelectUsers()
		for i := range users {
			users[i].Token = ""
			users[i].AES = ""
		}
		SetResponse(r, users)
	})
}

// RenameAccount переименовывает учетную запись
func RenameAccount() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := struct {
			Lastname   string `json:"lastname"`
			Firstname  string `json:"firstname"`
			Patronimyc string `json:"patronimyc"`
			Color      string `json:"color"`
		}{}
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		err := db.UpdateUserName(u.ID, req.Lastname, req.Firstname, req.Patronimyc)
		if err != nil {
			SetError(r, ErrUpdateData)
			return
		}
		err = db.UpdateColor(u.ID, req.Color)
		if err != nil {
			SetError(r, ErrUpdateData)
			return
		}
		SetResponse(r, struct{}{})
		gojlog.Infof("%s изменил(а) данные учетной записи", GetAccountInfo(r))
	})
}
