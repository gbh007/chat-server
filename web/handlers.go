package web

import (
	"app/db"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/gbh007/gojlog"
)

// FileWriteHandler хандлер для ответа в виде файла
func FileWriteHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// возвращаем только заголовок если это OPTIONS
		if r.Method == http.MethodOptions {
			return
		}
		// обрабатываем следующий хандлер
		if next != nil {
			next.ServeHTTP(w, r)
		}
		// обрабатываем ошибку если есть
		if err := r.Context().Err(); err != nil {
			w.Header().Set("Content-Type", "text/plain; charset=utf-8")
			switch err {
			case ErrForbidden:
				w.WriteHeader(http.StatusForbidden)
			case ErrNotFound:
				w.WriteHeader(http.StatusNotFound)
			case ErrNotAuth:
				w.WriteHeader(http.StatusUnauthorized)
			case ErrParseData:
				w.WriteHeader(http.StatusBadRequest)
			case ErrToLargeData:
				w.WriteHeader(http.StatusRequestEntityTooLarge)
			default:
				w.WriteHeader(http.StatusInternalServerError)
			}
			if _, err := io.WriteString(w, err.Error()); err != nil {
				gojlog.Error(err)
			}
			return
		}
		// установка типа вложения
		filename := r.Context().Value(ResponseFilenameKey)
		if filename != nil {
			w.Header().Set("Content-Disposition", `attachment; filename="`+fmt.Sprint(filename)+`"`)
		}
		// установка типа файла
		filetype := r.Context().Value(ResponseFileTypeKey)
		if filename != nil {
			w.Header().Set("Content-Type", fmt.Sprint(filetype))
		}
		w.WriteHeader(http.StatusOK)
		// получение данных
		data := r.Context().Value(ResponseDataKey)
		if data == nil {
			return
		}
		// определение типа данных
		switch f := data.(type) {
		case io.ReadCloser:
			defer f.Close()
			if _, err := io.Copy(w, f); err != nil {
				gojlog.Error(err)
			}
		case io.Reader:
			if _, err := io.Copy(w, f); err != nil {
				gojlog.Error(err)
			}
		}
	})
}

// JSONWriteHandler хандлер для ответа в виде json
func JSONWriteHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
		w.Header().Set("Content-Type", "application/json")
		if r.Method == http.MethodOptions {
			w.Header().Set("Allow", "POST, OPTIONS")
			return
		}
		if next != nil {
			next.ServeHTTP(w, r)
		}
		enc := json.NewEncoder(w)
		if err := r.Context().Err(); err != nil {
			switch err {
			case ErrForbidden:
				w.WriteHeader(http.StatusForbidden)
			case ErrNotFound:
				w.WriteHeader(http.StatusNotFound)
			case ErrNotAuth:
				w.WriteHeader(http.StatusUnauthorized)
			case ErrParseData:
				w.WriteHeader(http.StatusBadRequest)
			case ErrToLargeData:
				w.WriteHeader(http.StatusRequestEntityTooLarge)
			default:
				w.WriteHeader(http.StatusInternalServerError)
			}
			if err := enc.Encode(err.Error()); err != nil {
				gojlog.Error(err)
			}
			return
		}
		if tmp, ok := r.Context().Value(ResponseRedirectKey).(string); ok {
			http.Redirect(w, r, tmp, http.StatusTemporaryRedirect)
			return
		}
		w.WriteHeader(http.StatusOK)
		data := r.Context().Value(ResponseDataKey)
		if data == nil {
			return
		}
		if err := enc.Encode(data); err != nil {
			gojlog.Error(err)
		}
	})
}

// SessionHandler обработчик сессии пользователя
func SessionHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, err := CheckSession(r)
		if err == nil {
			// устанавливаем в контекст данные пользователя
			SetData(r, ResponseUserKey, u)
		}
		next.ServeHTTP(w, r)
	})
}

// AuthRequireHandler проверяет авторизован ли пользователь (отсекает 401 ошибкой не авторизованных)
func AuthRequireHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, login := GetSUPOk(r)
		if !login {
			SetError(r, ErrNotAuth)
			return
		}
		db.UpdateUserLA(u.ID)
		next.ServeHTTP(w, r)
	})
}

// AddHandler добавляет в мукс обработчик JSON
func AddHandler(mux *http.ServeMux, uri string, auth bool, next http.Handler) {
	if auth {
		mux.Handle(uri, JSONWriteHandler(SessionHandler(AuthRequireHandler(next))))
	} else {
		mux.Handle(uri, JSONWriteHandler(SessionHandler(next)))
	}
}

// AddFileHandler добавляет в мукс обработчик файлов
func AddFileHandler(mux *http.ServeMux, uri string, next http.Handler) {
	mux.Handle(uri, FileWriteHandler(SessionHandler(AuthRequireHandler(next))))
}
