package web

import (
	"app/db"
	"net/http"
	"time"
)

// privateMessage отправляет личное сообщение
func privateMessage(date time.Time, text string, senderID, reciverID int) error {
	m := db.Message{
		Date:      date,
		Text:      text,
		SenderID:  senderID,
		UserID:    reciverID,
		PrivateID: senderID,
	}
	err := db.InsertMessage(m)
	if err != nil {
		return err
	}
	if senderID != reciverID {
		m := db.Message{
			Date:      date,
			Text:      text,
			SenderID:  senderID,
			UserID:    senderID,
			PrivateID: reciverID,
		}
		err := db.InsertMessage(m)
		if err != nil {
			return err
		}
	}
	return nil
}

// groupMessage отправляет приватное сообщение
func groupMessage(date time.Time, text string, senderID, groupID int) error {
	users := db.SelectUsersInGroup(groupID)
	if len(users) == 0 {
		return ErrCreateData
	}
	t := time.Now()
	for _, user := range users {
		m := db.Message{
			Date:     t,
			Text:     text,
			SenderID: senderID,
			UserID:   user.ID,
			GroupID:  groupID,
		}
		err := db.InsertMessage(m)
		if err != nil {
			return err
		}
	}
	return nil
}

// PrivateMessage отправляет приватное сообщение
func PrivateMessage() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := struct {
			Text string `json:"text"`
			To   int    `json:"to"`
		}{}
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		err := privateMessage(time.Now(), req.Text, u.ID, req.To)
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		SetResponse(r, struct{}{})
	})
}

// PrivateAll возвращает список сообщений с указаным пользователем
func PrivateAll() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req int
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		SetResponse(r, db.SelectPrivateMessages(u.ID, req))
	})
}

// PrivateCount возвращает количество новых приватных сообщений
func PrivateCount() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, _ := GetSUPOk(r)
		SetResponse(r, db.SelectCountPrivateMessages(u.ID))
	})
}

// PrivateUpdateReaded обновляет список прочитаных сообщений
func PrivateUpdateReaded() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req int
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		err := db.UpdateReadPrivateMessages(u.ID, req)
		if err != nil {
			SetError(r, ErrUpdateData)
			return
		}
		SetResponse(r, struct{}{})
	})
}

// GroupMessage отправляет груповое сообщение
func GroupMessage() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := struct {
			Text string `json:"text"`
			To   int    `json:"to"`
		}{}
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		err := groupMessage(time.Now(), req.Text, u.ID, req.To)
		if err != nil {
			SetError(r, ErrCreateData)
			return
		}
		SetResponse(r, struct{}{})
	})
}

// GroupAll возвращает список сообщений с указаной группой
func GroupAll() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req int
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		SetResponse(r, db.SelectGroupMessages(u.ID, req))
	})
}

// GroupCount возвращает количество новых групповых сообщений
func GroupCount() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, _ := GetSUPOk(r)
		SetResponse(r, db.SelectCountGroupMessages(u.ID))
	})
}

// GroupUpdateReaded обновляет список прочитаных сообщений в группе
func GroupUpdateReaded() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var req int
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		u, _ := GetSUPOk(r)
		err := db.UpdateReadGroupMessages(u.ID, req)
		if err != nil {
			SetError(r, ErrUpdateData)
			return
		}
		SetResponse(r, struct{}{})
	})
}
